<?php

namespace App\Utiles\DTO;

use App\Utiles\ConfigUploader\ConfigUploaderInterface;
use App\Utiles\DTO\Interfaces\DTOInterface;
use App\Utiles\DTO\Exceptions\DTOBuilderException;
use App\Utiles\DTO\DTOBuilders\Factory as DTOsFactory;

/**
 * Class DTOBuilder
 * @package App\Utiles\DTOBuilder
 */
final class DTOBuilder
{
    /**
     * @var array
     */
    private $context;

    /**
     * @var ConfigUploaderInterface
     */
    private $configHelper;

    /**
     * @var DTOsFactory
     */
    private $DTOsFactory;

    /**
     * @var bool
     */
    private $checkDataKeys;

    /**
     * DTOBuilder constructor.
     * @param DTOsFactory $factory
     * @param ConfigUploaderInterface $configHelper
     */
    public function __construct(
        DTOsFactory $factory,
        ConfigUploaderInterface $configHelper
    ) {
        $this->DTOsFactory = $factory;
        $this->configHelper = $configHelper;
    }

    /**
     * @param string $context
     * @param array $data
     * @param bool $checkDataKeys
     * @return DTOInterface|array
     * @throws DTOBuilderException
     */
    public function getDTOs(string $context, array $data, bool $checkDataKeys = true)
    {
        $this->configure($context, $checkDataKeys);

        return $this->context['type'] == 'single'
            ? $this->buildDTO($this->context['DTOsAliases'], $data)
            : $this->buildDTOs($data);
    }

    /**
     * @param string $context
     * @param bool $checkDataKeys
     * @throws DTOBuilderException
     */
    private function configure(string $context, bool $checkDataKeys)
    {
        $this->checkDataKeys = $checkDataKeys;
        $this->context = $this->configHelper->upload('DTO.contexts.' . $context);
        if (is_null($this->context)) {
            throw new DTOBuilderException(
                sprintf('Context - "%s" was not found', $context)
            );
        }
    }

    /**
     * @param array $data
     * @return array
     * @throws DTOBuilderException
     */
    private function buildDTOs(array $data)
    {
        $DTOs = [];
        foreach ($this->context['DTOsAliases'] as $DTOAlias) {
            $DTOs[$DTOAlias] = $this->buildDTO($DTOAlias, $data);
        }

        return $DTOs;
    }

    /**
     * @param string $DTOBuilderAliases
     * @param array|null $data
     * @return DTOInterface|null
     * @throws DTOBuilderException
     */
    private function buildDTO(string $DTOBuilderAliases, array $data = null)
    {
        $data = $this->checkDataKeys ? $data[$DTOBuilderAliases] : $data;
        if (! $data) {
            return null;
        }

        return $this->DTOsFactory->build($DTOBuilderAliases, $data);
    }
}

<?php

namespace App\Utiles\DTO\DTOBuilders;

use App\Utiles\ConfigUploader\ConfigUploaderInterface;
use App\Utiles\DTO\Interfaces\DTOInterface;
use App\Utiles\DTO\Exceptions\DTOBuilderException;
use App\Utiles\DTO\Assemblers\Builder;

/**
 * Class Factory
 * @package App\Utiles\DTOBuilder\DTOBuilders
 */
final class Factory
{
    /**
     * @var array
     */
    private $DTOObjects = [];

    /**
     * Factory constructor.
     * @param ConfigUploaderInterface $configHelper
     */
    public function __construct(ConfigUploaderInterface $configHelper)
    {
        $this->configure($configHelper);
    }

    /**
     * @param ConfigUploaderInterface $configHelper
     */
    private function configure(ConfigUploaderInterface $configHelper)
    {
        $this->DTOObjects = $configHelper->upload('DTO.objects');
    }

    /**
     * @param string $DTOAlias
     * @param array $data
     * @return DTOInterface
     * @throws DTOBuilderException
     */
    public function build(string $DTOAlias, array $data): DTOInterface
    {
        try {
            $DTOData = $this->DTOObjects[$DTOAlias];
        } catch (\Throwable $e) {
            throw new DTOBuilderException(
                sprintf('DTO by "%s" alias was not found', $DTOAlias)
            );
        }

        return isset($DTOData['builder'])
            ? $this->buildWithAssembler($data, $DTOData)
            : $this->buildWithoutAssembler($data, $DTOData);
    }

    /**
     * @param $data
     * @param $DTOParts
     * @return DTOInterface
     */
    private function buildWithAssembler(array $data, $DTOParts): DTOInterface
    {
        /**
         * @var Builder $assembler
         */
        $assembler = new $DTOParts['builder']($DTOParts['object']);

        return $assembler->build($data);
    }

    /**
     * @param $data
     * @param $DTOParts
     * @return DTOInterface
     */
    private function buildWithoutAssembler(array $data, $DTOParts): DTOInterface
    {
        return new $DTOParts['object']($data);
    }
}

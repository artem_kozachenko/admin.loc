<?php

namespace App\Utiles\DTO\Assemblers;

use App\Utiles\DTO\Interfaces\BuilderInterface;
use App\Utiles\DTO\Interfaces\DTOInterface;

/**
 * Class Builder
 * @package App\Utiles\DTOBuilder\Assembler
 */
abstract class Builder implements BuilderInterface
{
    /**
     * Related object class name
     *
     * @var string
     */
    private $object;

    /**
     * Builder constructor.
     * @param string $object
     */
    public function __construct(string $object)
    {
        $this->object = $object;
    }

    /**
     * Building process
     *
     * @param array $data
     * @return mixed
     */
    abstract public function build(array $data);

    /**
     * Creates a instance of related object
     *
     * @return DTOInterface
     */
    protected function getObject() : DTOInterface
    {
        $object = $this->object;

        return new $object();
    }
}

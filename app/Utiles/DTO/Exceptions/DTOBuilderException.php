<?php

namespace App\Utiles\DTO\Exceptions;

use Exception;

/**
 * Class DTOBuilderException
 * @package App\Utiles\DTOBuilder\Exceptions
 */
final class DTOBuilderException extends Exception
{
    //
}

<?php

namespace App\Utiles\DTO\Interfaces;

/**
 * Interface BuilderInterface
 * @package App\Utiles\DTOBuilder\Interfaces
 */
interface BuilderInterface
{
    /**
     * @param array $data
     * @return mixed
     */
    public function build(array $data);
}

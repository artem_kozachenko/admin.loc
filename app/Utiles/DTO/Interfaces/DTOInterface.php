<?php

namespace App\Utiles\DTO\Interfaces;

/**
 * Interface DTOInterface
 * @package App\Utiles\DTOBuilder\Interfaces
 */
interface DTOInterface
{
    /**
     * @return array
     */
    public function toArray(): array;
}

<?php

namespace App\Utiles\Repository\QueryObject;

/**
 * Interface QueryObjectInterface
 * @package App\Utiles\Repository\QueryObject
 */
interface QueryObjectInterface
{
    /**
     * @return array|null
     */
    public function getSelect(): ?array;

    /**
     * @param array|null $select
     */
    public function setSelect(?array $select): void;

    /**
     * @return array|null
     */
    public function getWith(): ?array;

    /**
     * @param array|null $with
     */
    public function setWith(?array $with): void;

    /**
     * @return array|null
     */
    public function getWhere(): ?array;

    /**
     * @param array|null $where
     */
    public function setWhere(?array $where): void;

    /**
     * @return array|null
     */
    public function getWhereBetween(): ?array;

    /**
     * @param array|null $whereBetween
     */
    public function setWhereBetween(?array $whereBetween): void;

    /**
     * @return array|null
     */
    public function getWhereNotBetween(): ?array;

    /**
     * @param array|null $whereNotBetween
     */
    public function setWhereNotBetween(?array $whereNotBetween): void;

    /**
     * @return array|null
     */
    public function getWhereIn(): ?array;

    /**
     * @param array|null $whereIn
     */
    public function setWhereIn(?array $whereIn): void;

    /**
     * @return array|null
     */
    public function getWhereNotIn(): ?array;

    /**
     * @param array|null $whereNotIn
     */
    public function setWhereNotIn(?array $whereNotIn): void;

    /**
     * @return string|null
     */
    public function getWhereNull(): ?string;

    /**
     * @param string|null $whereNull
     */
    public function setWhereNull(?string $whereNull): void;

    /**
     * @return string|null
     */
    public function getWhereNotNull(): ?string;

    /**
     * @param string|null $whereNotNull
     */
    public function setWhereNotNull(?string $whereNotNull): void;

    /**
     * @return array|null
     */
    public function getWhereDate(): ?array;

    /**
     * @param array|null $whereDate
     */
    public function setWhereDate(?array $whereDate): void;

    /**
     * @return array|null
     */
    public function getWhereYear(): ?array;

    /**
     * @param array|null $whereYear
     */
    public function setWhereYear(?array $whereYear): void;

    /**
     * @return array|null
     */
    public function getWhereMonth(): ?array;

    /**
     * @param array|null $whereMonth
     */
    public function setWhereMonth(?array $whereMonth): void;

    /**
     * @return array|null
     */
    public function getWhereDay(): ?array;

    /**
     * @param array|null $whereDay
     */
    public function setWhereDay(?array $whereDay): void;

    /**
     * @return array|null
     */
    public function getWhereTime(): ?array;

    /**
     * @param array|null $whereTime
     */
    public function setWhereTime(?array $whereTime): void;

    /**
     * @return array|null
     */
    public function getWhereColumn(): ?array;

    /**
     * @param array|null $whereColumn
     */
    public function setWhereColumn(?array $whereColumn): void;

    /**
     * @return array|null
     */
    public function getWhereHas(): ?array;

    /**
     * @param array|null $whereHas
     */
    public function setWhereHas(?array $whereHas): void;

    /**
     * @return string|null
     */
    public function getHas(): ?string;

    /**
     * @param string|null $has
     */
    public function setHas(?string $has): void;

    /**
     * @return bool|null
     */
    public function getDistinct(): ?bool;

    /**
     * @param bool|null $distinct
     */
    public function setDistinct(?bool $distinct): void;

    /**
     * @return string|null
     */
    public function getOrderBy(): ?string;

    /**
     * @param string|null $orderBy
     */
    public function setOrderBy(?string $orderBy): void;

    /**
     * @return string|null
     */
    public function getGroupBy(): ?string;

    /**
     * @param string|null $groupBy
     */
    public function setGroupBy(?string $groupBy): void;

    /**
     * @return array|null
     */
    public function getHaving(): ?array;

    /**
     * @param array|null $having
     */
    public function setHaving(?array $having): void;

    /**
     * @return int|null
     */
    public function getLimit(): ?int;

    /**
     * @param int|null $limit
     */
    public function setLimit(?int $limit): void;

    /**
     * @return int|null
     */
    public function getOffset(): ?int;

    /**
     * @param int|null $offset
     */
    public function setOffset(?int $offset): void;

    /**
     * @return int|null
     */
    public function getPerPage(): ?int;

    /**
     * @param int|null $perPage
     */
    public function setPerPage(?int $perPage): void;
}

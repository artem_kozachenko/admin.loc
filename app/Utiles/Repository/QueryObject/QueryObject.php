<?php

namespace App\Utiles\Repository\QueryObject;

/**
 * Class QueryObject
 * @package App\Utiles\Repository\QueryObject
 */
abstract class QueryObject implements QueryObjectInterface
{
    /**
     * @var array|null
     */
    private $select = [];

    /**
     * @var array|null
     */
    private $with = [];

    /**
     * @var array|null
     */
    private $where = [];

    /**
     * @var array|null
     */
    private $whereBetween = [];

    /**
     * @var array|null
     */
    private $whereNotBetween = [];

    /**
     * @var array|null
     */
    private $whereIn = [];

    /**
     * @var array|null
     */
    private $whereNotIn = [];

    /**
     * @var string|null
     */
    private $whereNull;

    /**
     * @var string|null
     */
    private $whereNotNull;

    /**
     * @var array|null
     */
    private $whereDate = [];

    /**
     * @var array|null
     */
    private $whereYear = [];

    /**
     * @var array|null
     */
    private $whereMonth = [];

    /**
     * @var array|null
     */
    private $whereDay = [];

    /**
     * @var array|null
     */
    private $whereTime = [];

    /**
     * @var array|null
     */
    private $whereColumn = [];

    /**
     * @var array|null
     */
    private $whereHas = [];

    /**
     * @var string|null
     */
    private $has;

    /**
     * @var bool|null
     */
    private $distinct = false;

    /**
     * @var string|null
     */
    private $orderBy;

    /**
     * @var string|null
     */
    private $groupBy;

    /**
     * @var array|null
     */
    private $having = [];

    /**
     * @var int|null
     */
    private $limit;

    /**
     * @var int|null
     */
    private $offset;

    /**
     * @var int|null
     */
    private $perPage;

    /**
     * @return array|null
     */
    public function getSelect(): ?array
    {
        return $this->select;
    }

    /**
     * @param array|null $select
     */
    public function setSelect(?array $select): void
    {
        $this->select = $select;
    }

    /**
     * @return array|null
     */
    public function getWith(): ?array
    {
        return $this->with;
    }

    /**
     * @param array|null $with
     */
    public function setWith(?array $with): void
    {
        $this->with = $with;
    }

    /**
     * @return array|null
     */
    public function getWhere(): ?array
    {
        return $this->where;
    }

    /**
     * @param array|null $where
     */
    public function setWhere(?array $where): void
    {
        $this->where = $where;
    }

    /**
     * @param string $index
     * @param $value
     * @return int
     */
    protected function addToWhere(string $index, $value)
    {
        return array_push($this->where, [$index, '=', $value]);
    }

    /**
     * @return array|null
     */
    public function getWhereBetween(): ?array
    {
        return $this->whereBetween;
    }

    /**
     * @param array|null $whereBetween
     */
    public function setWhereBetween(?array $whereBetween): void
    {
        $this->whereBetween = $whereBetween;
    }

    /**
     * @return array|null
     */
    public function getWhereNotBetween(): ?array
    {
        return $this->whereNotBetween;
    }

    /**
     * @param array|null $whereNotBetween
     */
    public function setWhereNotBetween(?array $whereNotBetween): void
    {
        $this->whereNotBetween = $whereNotBetween;
    }

    /**
     * @return array|null
     */
    public function getWhereIn(): ?array
    {
        return $this->whereIn;
    }

    /**
     * @param array|null $whereIn
     */
    public function setWhereIn(?array $whereIn): void
    {
        $this->whereIn = $whereIn;
    }

    /**
     * @return array|null
     */
    public function getWhereNotIn(): ?array
    {
        return $this->whereNotIn;
    }

    /**
     * @param array|null $whereNotIn
     */
    public function setWhereNotIn(?array $whereNotIn): void
    {
        $this->whereNotIn = $whereNotIn;
    }

    /**
     * @return string|null
     */
    public function getWhereNull(): ?string
    {
        return $this->whereNull;
    }

    /**
     * @param string|null $whereNull
     */
    public function setWhereNull(?string $whereNull): void
    {
        $this->whereNull = $whereNull;
    }

    /**
     * @return string|null
     */
    public function getWhereNotNull(): ?string
    {
        return $this->whereNotNull;
    }

    /**
     * @param string|null $whereNotNull
     */
    public function setWhereNotNull(?string $whereNotNull): void
    {
        $this->whereNotNull = $whereNotNull;
    }

    /**
     * @return array|null
     */
    public function getWhereDate(): ?array
    {
        return $this->whereDate;
    }

    /**
     * @param array|null $whereDate
     */
    public function setWhereDate(?array $whereDate): void
    {
        $this->whereDate = $whereDate;
    }

    /**
     * @return array|null
     */
    public function getWhereYear(): ?array
    {
        return $this->whereYear;
    }

    /**
     * @param array|null $whereYear
     */
    public function setWhereYear(?array $whereYear): void
    {
        $this->whereYear = $whereYear;
    }

    /**
     * @return array|null
     */
    public function getWhereMonth(): ?array
    {
        return $this->whereMonth;
    }

    /**
     * @param array|null $whereMonth
     */
    public function setWhereMonth(?array $whereMonth): void
    {
        $this->whereMonth = $whereMonth;
    }

    /**
     * @return array|null
     */
    public function getWhereDay(): ?array
    {
        return $this->whereDay;
    }

    /**
     * @param array|null $whereDay
     */
    public function setWhereDay(?array $whereDay): void
    {
        $this->whereDay = $whereDay;
    }

    /**
     * @return array|null
     */
    public function getWhereTime(): ?array
    {
        return $this->whereTime;
    }

    /**
     * @param array|null $whereTime
     */
    public function setWhereTime(?array $whereTime): void
    {
        $this->whereTime = $whereTime;
    }

    /**
     * @return array|null
     */
    public function getWhereColumn(): ?array
    {
        return $this->whereColumn;
    }

    /**
     * @param array|null $whereColumn
     */
    public function setWhereColumn(?array $whereColumn): void
    {
        $this->whereColumn = $whereColumn;
    }

    /**
     * @return array|null
     */
    public function getWhereHas(): ?array
    {
        return $this->whereHas;
    }

    /**
     * @param array|null $whereHas
     */
    public function setWhereHas(?array $whereHas): void
    {
        $this->whereHas = $whereHas;
    }

    /**
     * @return string|null
     */
    public function getHas(): ?string
    {
        return $this->has;
    }

    /**
     * @param string|null $has
     */
    public function setHas(?string $has): void
    {
        $this->has = $has;
    }

    /**
     * @return bool|null
     */
    public function getDistinct(): ?bool
    {
        return $this->distinct;
    }

    /**
     * @param bool|null $distinct
     */
    public function setDistinct(?bool $distinct): void
    {
        $this->distinct = $distinct;
    }

    /**
     * @return string|null
     */
    public function getOrderBy(): ?string
    {
        return $this->orderBy;
    }

    /**
     * @param string|null $orderBy
     */
    public function setOrderBy(?string $orderBy): void
    {
        $this->orderBy = $orderBy;
    }

    /**
     * @return string|null
     */
    public function getGroupBy(): ?string
    {
        return $this->groupBy;
    }

    /**
     * @param string|null $groupBy
     */
    public function setGroupBy(?string $groupBy): void
    {
        $this->groupBy = $groupBy;
    }

    /**
     * @return array|null
     */
    public function getHaving(): ?array
    {
        return $this->having;
    }

    /**
     * @param array|null $having
     */
    public function setHaving(?array $having): void
    {
        $this->having = $having;
    }

    /**
     * @return int|null
     */
    public function getLimit(): ?int
    {
        return $this->limit;
    }

    /**
     * @param int|null $limit
     */
    public function setLimit(?int $limit): void
    {
        $this->limit = $limit;
    }

    /**
     * @return int|null
     */
    public function getOffset(): ?int
    {
        return $this->offset;
    }

    /**
     * @param int|null $offset
     */
    public function setOffset(?int $offset): void
    {
        $this->offset = $offset;
    }

    /**
     * @return int|null
     */
    public function getPerPage(): ?int
    {
        return $this->perPage;
    }

    /**
     * @param int|null $perPage
     */
    public function setPerPage(?int $perPage): void
    {
        $this->perPage = $perPage;
    }
}

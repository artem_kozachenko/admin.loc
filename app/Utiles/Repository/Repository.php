<?php

namespace App\Utiles\Repository;

use App\Utiles\Repository\QueryObject\QueryObjectInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class Repository
 * @package App\Utiles\Repository
 */
abstract class Repository implements RepositoryInterface
{
    /**
     * @var Builder
     */
    private $queryBuilder;

    /**
     * Repository constructor.
     */
    public function __construct()
    {
        $this->queryBuilder = $this->setQueryBuilder();
    }

    /**
     * @return Builder
     */
    abstract protected function setQueryBuilder(): Builder;

    /**
     * @param QueryObjectInterface $queryObject
     * @return Collection|LengthAwarePaginator
     */
    public function getByQueryObject(QueryObjectInterface $queryObject)
    {
        $query = $this->buildByQueryObject($queryObject);
        $perPage = $queryObject->getPerPage();

        return $perPage
            ? $query->paginate($perPage)
            : $query->get();
    }

    /**
     * @param QueryObjectInterface $queryObject
     * @return Builder
     */
    private function buildByQueryObject(QueryObjectInterface $queryObject): Builder
    {
        return $this
            ->select($queryObject->getSelect())
            ->with($queryObject->getWith())
            ->where($queryObject->getWhere())
            ->whereBetween($queryObject->getWhereBetween())
            ->whereNotBetween($queryObject->getWhereNotBetween())
            ->whereIn($queryObject->getWhereIn())
            ->whereNotIn($queryObject->getwhereNotIn())
            ->whereNull($queryObject->getWhereNull())
            ->whereNotNull($queryObject->getWhereNotNull())
            ->whereDate($queryObject->getWhereDate())
            ->whereYear($queryObject->getWhereYear())
            ->whereMonth($queryObject->getWhereMonth())
            ->whereDay($queryObject->getWhereDay())
            ->whereTime($queryObject->getWhereTime())
            ->whereColumn($queryObject->getWhereColumn())
            ->whereHas($queryObject->getWhereHas())
            ->has($queryObject->getHas())
            ->distinct($queryObject->getDistinct())
            ->orderBy($queryObject->getOrderBy())
            ->groupBy($queryObject->getGroupBy())
            ->having($queryObject->getHaving())
            ->offset($queryObject->getOffset())
            ->limit($queryObject->getLimit())
            ->returnQueryBuilder();
    }

    /**
     * @param array $select
     * @return $this
     */
    private function select(array $select = [])
    {
        if ($select) {
            $this->queryBuilder->select($select);
        }

        return $this;
    }

    /**
     * @param array $with
     * @return $this
     */
    private function with(array $with = [])
    {
        if ($with) {
            $this->queryBuilder->with($with);
        }

        return $this;
    }

    /**
     * @param array $where
     * @return $this
     */
    private function where(array $where = [])
    {
        if ($where) {
            $this->queryBuilder->where($where);
        }

        return $this;
    }

    /**
     * @param array $whereBetween
     * @return $this
     */
    private function whereBetween(array $whereBetween = [])
    {
        if ($whereBetween) {
            $this->queryBuilder->whereBetween($whereBetween['field'], $whereBetween['range']);
        }

        return $this;
    }

    /**
     * @param array $whereNotBetween
     * @return $this
     */
    private function whereNotBetween(array $whereNotBetween = [])
    {
        if ($whereNotBetween) {
            $this->queryBuilder->whereNotBetween($whereNotBetween['field'], $whereNotBetween['range']);
        }

        return $this;
    }

    /**
     * @param array $whereIn
     * @return $this
     */
    private function whereIn(array $whereIn = [])
    {
        if ($whereIn) {
            $this->queryBuilder->whereIn($whereIn['field'], $whereIn['range']);
        }

        return $this;
    }

    /**
     * @param array $whereNotIn
     * @return $this
     */
    private function whereNotIn(array $whereNotIn = [])
    {
        if ($whereNotIn) {
            $this->queryBuilder->whereNotIn($whereNotIn['field'], $whereNotIn['range']);
        }

        return $this;
    }

    /**
     * @param string|null $field
     * @return $this
     */
    private function whereNull(string $field = null)
    {
        if ($field) {
            $this->queryBuilder->whereNull($field);
        }

        return $this;
    }

    /**
     * @param string|null $field
     * @return $this
     */
    private function whereNotNull(string $field = null)
    {
        if ($field) {
            $this->queryBuilder->whereNotNull($field);
        }

        return $this;
    }

    /**
     * @param array $whereDate
     * @return $this
     */
    private function whereDate(array $whereDate = [])
    {
        if ($whereDate) {
            $this->queryBuilder->whereDate($whereDate['column'], $whereDate['value']);
        }

        return $this;
    }

    /**
     * @param array $whereYear
     * @return $this
     */
    private function whereYear(array $whereYear = [])
    {
        if ($whereYear) {
            $this->queryBuilder->whereYear($whereYear['column'], $whereYear['value']);
        }

        return $this;
    }

    /**
     * @param array $whereMonth
     * @return $this
     */
    private function whereMonth(array $whereMonth = [])
    {
        if ($whereMonth) {
            $this->queryBuilder->whereMonth($whereMonth['column'], $whereMonth['value']);
        }

        return $this;
    }

    /**
     * @param array $whereDay
     * @return $this
     */
    private function whereDay(array $whereDay = [])
    {
        if ($whereDay) {
            $this->queryBuilder->whereDay($whereDay['column'], $whereDay['value']);
        }

        return $this;
    }

    /**
     * @param array $whereTime
     * @return $this
     */
    private function whereTime(array $whereTime = [])
    {
        if ($whereTime) {
            $this->queryBuilder->whereTime($whereTime['column'], $whereTime['value']);
        }

        return $this;
    }

    /**
     * @param array $whereColumn
     * @return $this
     */
    private function whereColumn(array $whereColumn = [])
    {
        if ($whereColumn) {
            $this->queryBuilder->whereColumn($whereColumn);
        }

        return $this;
    }

    /**
     * @param array $whereHas
     * @return $this
     */
    private function whereHas(array $whereHas = [])
    {
        if ($whereHas) {
            $this->queryBuilder->whereHas($whereHas['relation'], $whereHas['callback']);
        }

        return $this;
    }

    /**
     * @param string|null $has
     * @return $this
     */
    private function has(string $has = null)
    {
        if ($has) {
            $this->queryBuilder->has($has);
        }

        return $this;
    }

    /**
     * @param bool $distinct
     * @return $this
     */
    private function distinct(bool $distinct = false)
    {
        if ($distinct) {
            $this->queryBuilder->distinct();
        }

        return $this;
    }

    /**
     * @param string|null $orderBy
     * @return $this
     */
    private function orderBy(string $orderBy = null)
    {
        if ($orderBy) {
            $this->queryBuilder->orderBy($orderBy);
        }

        return $this;
    }

    /**
     * @param string|null $groupBy
     * @return $this
     */
    private function groupBy(string $groupBy = null)
    {
        if ($groupBy) {
            $this->queryBuilder->groupBy($groupBy);
        }

        return $this;
    }

    /**
     * @param array $having
     * @return $this
     */
    private function having(array $having = [])
    {
        if ($having) {
            $this->queryBuilder->having($having['column'], $having['operator'] ?? '=', $having['value']);
        }

        return $this;
    }

    /**
     * @param int|null $offset
     * @return $this
     */
    private function offset(int $offset = null)
    {
        if ($offset) {
            $this->queryBuilder->offset($offset);
        }

        return $this;
    }

    /**
     * @param int|null $limit
     * @return $this
     */
    private function limit(int $limit = null)
    {
        if ($limit) {
            $this->queryBuilder->limit($limit);
        }

        return $this;
    }

    /**
     * @return Builder
     */
    private function returnQueryBuilder(): Builder
    {
        return $this->queryBuilder;
    }
}

<?php

namespace App\Utiles\Repository;

use App\Utiles\Repository\QueryObject\QueryObjectInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

/**
 * Interface RepositoryInterface
 * @package App\Repositories
 */
interface RepositoryInterface
{
    /**
     * @param QueryObjectInterface $queryObject
     * @return Collection|LengthAwarePaginator
     */
    public function getByQueryObject(QueryObjectInterface $queryObject);
}

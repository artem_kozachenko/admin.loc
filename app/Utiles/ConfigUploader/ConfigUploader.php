<?php

namespace App\Utiles\ConfigUploader;

/**
 * Class ConfigUploader
 * @package App\Utiles\ConfigUploader
 */
final class ConfigUploader implements ConfigUploaderInterface
{
    /**
     * @param string $alias
     * @return array|null
     */
    public function upload(string $alias)
    {
        return config($alias);
    }
}

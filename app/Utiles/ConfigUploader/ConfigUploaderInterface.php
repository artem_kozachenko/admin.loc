<?php

namespace App\Utiles\ConfigUploader;

/**
 * Interface ConfigUploaderInterface
 * @package App\Utiles\ConfigUploader
 */
interface ConfigUploaderInterface
{
    public function upload(string $alias);
}

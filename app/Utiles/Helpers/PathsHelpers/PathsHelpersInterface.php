<?php

namespace App\Utiles\Helpers\Paths;

/**
 * Interface PathsHelpersInterface
 * @package App\Utiles\Helpers\PathsHelpers
 */
interface PathsHelpersInterface
{
    /**
     * @param string $path
     * @return string
     */
    public function appPath(string $path = ''): string;

    /**
     * @param string $path
     * @return string
     */
    public function basePath(string $path = ''): string;

    /**
     * @param string $path
     * @return string
     */
    public function configPath(string $path = ''): string;

    /**
     * @param string $path
     * @return string
     */
    public function databasePath(string $path = ''): string;

    /**
     * @param string $path
     * @param string $manifestDirectory
     * @return string
     * @throws \Exception
     */
    public function mix(string $path, string $manifestDirectory = ''): string;

    /**
     * @param string $path
     * @return string
     */
    public function publicPath(string $path = ''): string;

    /**
     * @param string $path
     * @return string
     */
    public function resourcePath(string $path = ''): string;

    /**
     * @param string $path
     * @return string
     */
    public function storagePath(string $path = ''): string;
}

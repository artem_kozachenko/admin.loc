<?php

namespace App\Utiles\Helpers\PathsHelpers;

use App\Utiles\Helpers\Paths\PathsHelpersInterface;

/**
 * Class PathsHelpers
 * @package App\Utiles\Helpers\PathsHelpers
 */
final class PathsHelpers implements PathsHelpersInterface
{
    /**
     * @param string $path
     * @return string
     */
    public function appPath(string $path = ''): string
    {
        return app_path($path);
    }

    /**
     * @param string $path
     * @return string
     */
    public function basePath(string $path = ''): string
    {
        return base_path($path);
    }

    /**
     * @param string $path
     * @return string
     */
    public function configPath(string $path = ''): string
    {
        return config_path($path);
    }

    /**
     * @param string $path
     * @return string
     */
    public function databasePath(string $path = ''): string
    {
        return database_path($path);
    }

    /**
     * @param string $path
     * @param string $manifestDirectory
     * @return string
     * @throws \Exception
     */
    public function mix(string $path, string $manifestDirectory = ''): string
    {
        return mix($path, $manifestDirectory);
    }

    /**
     * @param string $path
     * @return string
     */
    public function publicPath(string $path = ''): string
    {
        return public_path($path);
    }

    /**
     * @param string $path
     * @return string
     */
    public function resourcePath(string $path = ''): string
    {
        return resource_path($path);
    }

    /**
     * @param string $path
     * @return string
     */
    public function storagePath(string $path = ''): string
    {
        return resource_path($path);
    }
}

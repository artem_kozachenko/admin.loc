<?php

namespace App\Utiles\Serializer\Exceptions;

use Exception;

/**
 * Class SerializerException
 * @package App\Exceptions
 */
final class SerializerException extends Exception
{
    //
}

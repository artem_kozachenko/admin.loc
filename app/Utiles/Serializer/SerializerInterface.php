<?php

namespace App\Utiles\Serializer;

/**
 * Interface SerializerInterface
 * @package App\Utiles\Serializer
 */
interface SerializerInterface
{
    /**
     * @param array $array
     * @param string $objectAlias
     * @return mixed
     */
    public function deserialize(array $array, string $objectAlias);
}

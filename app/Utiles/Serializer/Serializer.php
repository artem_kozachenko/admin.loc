<?php

namespace App\Utiles\Serializer;

use App\Utiles\Resolve\ResolveHelperInterface;
use App\Utiles\ConfigUploader\ConfigUploaderInterface;
use App\Utiles\Serializer\Exceptions\SerializerException;
use App\Utiles\Serializer\Interfaces\SerializableObjectInterface;

/**
 * Class Serializer
 * @package App\Utiles\Serializer
 */
final class Serializer implements SerializerInterface
{
    /**
     * @var ResolveHelperInterface
     */
    private $resolver;

    /**
     * @var ConfigUploaderInterface
     */
    private $configUploader;

    /**
     * @var array
     */
    private $objectBuildingMap;

    /**
     * Serializer constructor.
     * @param ResolveHelperInterface $resolver
     * @param ConfigUploaderInterface $configUploader
     */
    public function __construct(ResolveHelperInterface $resolver, ConfigUploaderInterface $configUploader)
    {
        $this->resolver = $resolver;
        $this->configUploader = $configUploader;
    }

    /**
     * @param array $array
     * @param string $objectAlias
     * @return SerializableObjectInterface|mixed
     * @throws SerializerException
     */
    public function deserialize(array $array, string $objectAlias)
    {
        $this->configure($objectAlias);
        $object = $this->getObject($objectAlias);

        return $this->buildObject($array, $object);
    }

    /**
     * @param string $objectAlias
     * @throws SerializerException
     */
    private function configure(string $objectAlias)
    {
        try {
            $config = $this->configUploader->upload('serializer');
            $object = $config['objects'][$objectAlias];

            $this->objectBuildingMap = isset($object['group'])
                ? $this->getMapWithGroup($config, $object)
                : $object['map'];
        } catch (\Throwable $exception) {
            throw new SerializerException('Object "' . $objectAlias . '" was not built!');
        }
    }

    /**
     * @param array $config
     * @param array $object
     * @return array
     */
    private function getMapWithGroup(array $config, array $object)
    {
        $defaultGroupMap = $config['groups'][$object['group']];

        return array_merge($object['map'], $defaultGroupMap);
    }

    /**
     * @param string $objectAlias
     * @return mixed
     */
    private function getObject(string $objectAlias)
    {
        return $this->resolver->resolve($objectAlias);
    }

    /**
     * @param array $array
     * @param SerializableObjectInterface $object
     * @return SerializableObjectInterface
     */
    private function buildObject(array $array, SerializableObjectInterface $object)
    {
        foreach ($this->objectBuildingMap as $arrayKey => $functionAlias) {
            if (isset($array[$arrayKey])) {
                $object->{$functionAlias}($array[$arrayKey]);
            }
        }

        return $object;
    }
}

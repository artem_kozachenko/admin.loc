<?php

namespace App\Utiles\Validator;

use App\Utiles\Validator\Interfaces\ValidatorInterface;
use App\Utiles\Validator\Interfaces\ValidatorCompositeInterface;

/**
 * Class ValidatorComposite
 * @package App\Utiles\Validator
 */
final class ValidatorComposite implements ValidatorCompositeInterface
{
    /**
     * @var array
     */
    private $validators = [];

    /**
     * @param ValidatorInterface $validator
     */
    public function addValidator(ValidatorInterface $validator)
    {
        $this->validators[] = $validator;
    }

    /**
     * @param $data
     * @return bool
     */
    public function validate($data): bool
    {
        foreach ($this->validators as $validator) {
            if (! $validator->validate($data)) {
                return false;
            }
        }

        return true;
    }
}

<?php

namespace App\Utiles\Validator;

use App\Utiles\Resolve\ResolveHelperInterface;
use App\Utiles\ConfigUploader\ConfigUploaderInterface;
use App\Utiles\Validator\Exceptions\ValidatorException;
use App\Utiles\Validator\Interfaces\ValidatorInterface;
use App\Utiles\Validator\Interfaces\ConfigurableInterface;
use App\Utiles\Validator\Interfaces\ValidatorCompositeInterface;

/**
 * Class Factory
 * @package App\Utiles\Validator
 */
final class Factory
{
    /**
     * @var array
     */
    private $contexts;

    /**
     * @var array
     */
    private $validators;

    /**
     * @var array
     */
    private $currentContext;

    /**
     * @var ResolveHelperInterface
     */
    private $resolveHelper;

    /**
     * Factory constructor.
     * @param ConfigUploaderInterface $configHelper
     * @param ResolveHelperInterface $resolveHelper
     */
    public function __construct(
        ConfigUploaderInterface $configHelper,
        ResolveHelperInterface $resolveHelper
    ) {
        $this->resolveHelper = $resolveHelper;
        $this->configure($configHelper);
    }

    /**
     * @param ConfigUploaderInterface $configHelper
     */
    private function configure(ConfigUploaderInterface $configHelper)
    {
        $this->validators = $configHelper->upload('validator.validators');
        $this->contexts = $configHelper->upload('validator.contexts');
    }

    /**
     * @param string $context
     * @param string $configsStructureName
     * @return ValidatorInterface
     * @throws ValidatorException
     */
    public function getValidators(string $context, string $configsStructureName = 'default'): ValidatorInterface
    {
        $this->setCurrentContext($context);
        $validator = $this->buildValidator($this->currentContext['validator'], $configsStructureName);

        return $validator instanceof ValidatorCompositeInterface
            ? $this->fillComposite($validator, $configsStructureName)
            : $validator;
    }

    /**
     * @param string $context
     * @throws ValidatorException
     */
    private function setCurrentContext(string $context)
    {
        try {
            $this->currentContext = $this->contexts[$context];
        } catch (\Throwable $e) {
            throw new ValidatorException(
                sprintf('Context - "%s" was not found', $context)
            );
        }
    }

    /**
     * @param ValidatorCompositeInterface $validatorComposite
     * @param string $configsStructureName
     * @return ValidatorInterface
     * @throws ValidatorException
     */
    private function fillComposite(
        ValidatorCompositeInterface $validatorComposite,
        string $configsStructureName
    ): ValidatorInterface {
        if (empty($this->currentContext['validatorsAliases'])) {
            throw new ValidatorException('Validator composite parts were not specified');
        }
        foreach ($this->currentContext['validatorsAliases'] as $validatorAlias) {
            $validator = $this->buildValidator($validatorAlias, $configsStructureName);
            $validatorComposite->addValidator($validator);
        }

        return $validatorComposite;
    }

    /**
     * @param string $validatorAlias
     * @param string $configsStructureName
     * @return ValidatorInterface
     * @throws ValidatorException
     */
    private function buildValidator(string $validatorAlias, string $configsStructureName): ValidatorInterface
    {
        /**
         * @var ValidatorInterface $validator
         */
        try {
            $validator = $this->resolveHelper->resolve($this->validators[$validatorAlias]);
            if ($validator instanceof ConfigurableInterface) {
                $configs = $this->getConfigs($validatorAlias, $configsStructureName);
                $validator->setConfigs($configs);
            }

            return $validator;
        } catch (\Throwable $e) {
            throw new ValidatorException(
                sprintf('Validator with alias - "%s" was not found', $validatorAlias)
            );
        }
    }

    /**
     * @param string $validatorAlias
     * @param string $configsStructureName
     * @return mixed
     * @throws ValidatorException
     */
    private function getConfigs(string $validatorAlias, string $configsStructureName)
    {
        try {
            return
                $this->currentContext['configs'][$configsStructureName][$validatorAlias] ??
                $this->currentContext['configs']['default'][$validatorAlias];
        } catch (\Throwable $e) {
            throw new ValidatorException(
                sprintf('Configs for validator with alias - "%s" were not specified', $validatorAlias)
            );
        }
    }
}

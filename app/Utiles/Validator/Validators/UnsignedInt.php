<?php

namespace App\Utiles\Validator\Validators;

use App\Utiles\Validator\Interfaces\ConfigurableInterface;

/**
 * Class UnsignedInt
 * @package App\Utiles\Validator\Modules
 */
final class UnsignedInt extends Validator implements ConfigurableInterface
{
    /**
     * @param $data
     * @return bool
     */
    public function validate($data): bool
    {
        foreach ($this->configs as $config) {
            if ((int) $data[$config] < 0) {
                return false;
            }
        }

        return true;
    }
}

<?php

namespace App\Utiles\Validator\Validators;

use App\Utiles\Validator\Interfaces\ConfigurableInterface;

/**
 * Class MinValue
 * @package App\Utiles\Validator\Validators
 */
final class MinValue extends Validator implements ConfigurableInterface
{
    /**
     * @param $data
     * @return bool
     */
    public function validate($data): bool
    {
        foreach ($this->configs as $config) {
            if ($data[$config['field']] < $config['value']) {
                return false;
            }
        }

        return true;
    }
}

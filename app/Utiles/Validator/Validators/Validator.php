<?php

namespace App\Utiles\Validator\Validators;

use App\Utiles\Validator\Interfaces\ValidatorInterface;

/**
 * Class Validator
 * @package App\Utiles\Validator\Validators
 */
abstract class Validator implements ValidatorInterface
{
    /**
     * @var
     */
    protected $configs;

    /**
     * @param array $configs
     */
    public function setConfigs(array $configs)
    {
        $this->configs = $configs;
    }
}

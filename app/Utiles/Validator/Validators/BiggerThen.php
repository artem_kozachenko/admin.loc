<?php

namespace App\Utiles\Validator\Validators;

use App\Utiles\Validator\Interfaces\ConfigurableInterface;

/**
 * Class BiggerThen
 * @package App\Utiles\Validator\Validators
 */
final class BiggerThen extends Validator implements ConfigurableInterface
{
    /**
     * @param $data
     * @return bool
     */
    public function validate($data): bool
    {
        foreach ($this->configs as $config) {
            if ($data[$config['bigger']] < $data[$config['smaller']]) {
                return false;
            }
        }

        return true;
    }
}

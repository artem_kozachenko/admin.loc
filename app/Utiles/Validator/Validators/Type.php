<?php

namespace App\Utiles\Validator\Validators;

use App\Utiles\Validator\Interfaces\ConfigurableInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * Class Type
 * @package App\Utiles\Validator\Validators
 */
final class Type extends Validator implements ConfigurableInterface
{
    /**
     * @param $data
     * @return bool
     */
    public function validate($data): bool
    {
        foreach ($this->configs as $availableTypes) {
            if ($this->{$availableTypes}($data)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $data
     * @return bool
     */
    private function isArray($data): bool
    {
        return is_array($data);
    }

    /**
     * @param $data
     * @return bool
     */
    private function isLengthAwarePaginator($data): bool
    {
        return $data instanceof LengthAwarePaginator;
    }

    /**
     * @param $data
     * @return bool
     */
    private function isModel($data): bool
    {
        return $data instanceof Model;
    }

    /**
     * @param $data
     * @return bool
     */
    private function isCollection($data): bool
    {
        return $data instanceof Collection;
    }
}

<?php

namespace App\Utiles\Validator\Validators;

use App\Utiles\Validator\Interfaces\ConfigurableInterface;

/**
 * Class Required
 * @package App\Utiles\Validator\Modules
 */
final class Required extends Validator implements ConfigurableInterface
{
    /**
     * @param $data
     * @return bool
     */
    public function validate($data): bool
    {
        foreach ($this->configs as $requiredField) {
            if (! $this->isFieldIsset($data, $requiredField) || $this->isFieldEmpty($data, $requiredField)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param array $data
     * @param string $requiredField
     * @return bool
     */
    protected function getResponse(array $data, $requiredField): bool
    {
        return $this->isFieldIsset($data, $requiredField) || ! $this->isFieldEmpty($data, $requiredField);
    }

    /**
     * @param array $data
     * @param string $requiredField
     * @return bool
     */
    private function isFieldIsset(array $data, string $requiredField)
    {
        return isset($data[$requiredField]);
    }

    /**
     * @param array $data
     * @param string $requiredField
     * @return bool
     */
    private function isFieldEmpty(array $data, string $requiredField)
    {
        return empty(trim($data[$requiredField]));
    }
}

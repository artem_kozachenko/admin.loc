<?php

namespace App\Utiles\Validator\Interfaces;

/**
 * Interface ConfigurableInterface
 * @package App\Utiles\Validator\Interfaces
 */
interface ConfigurableInterface
{
    /**
     * @param array $configs
     * @return mixed
     */
    public function setConfigs(array $configs);
}

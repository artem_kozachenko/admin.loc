<?php

namespace App\Utiles\Validator\Interfaces;

/**
 * Interface ValidatorInterface
 * @package App\Utiles\Validator\Interfaces
 */
interface ValidatorInterface
{
    /**
     * @param $data
     * @return bool
     */
    public function validate($data): bool;
}

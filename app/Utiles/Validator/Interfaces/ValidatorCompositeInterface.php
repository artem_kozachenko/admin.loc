<?php

namespace App\Utiles\Validator\Interfaces;

/**
 * Interface ValidatorCompositeInterface
 * @package App\Utiles\Validator\Interfaces
 */
interface ValidatorCompositeInterface extends ValidatorInterface
{
    /**
     * @param ValidatorInterface $validator
     * @return mixed
     */
    public function addValidator(ValidatorInterface $validator);
}

<?php

namespace App\Utiles\Validator\Exceptions;

use Exception;

/**
 * Class ValidatorException
 * @package App\Exceptions
 */
final class ValidatorException extends Exception
{
    //
}

<?php

namespace App\Utiles\Hash;

/**
 * Interface HashInterface
 * @package App\Utiles\Hash
 */
interface HashInterface
{
    /**
     * @param string $string
     * @param array $options
     * @return string
     */
    public function make(string $string, array $options = []);

    /**
     * @param string $string
     * @param string $hashedString
     * @param array $options
     * @return bool
     */
    public function check(string $string, string $hashedString, array $options = []): bool;
}

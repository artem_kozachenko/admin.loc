<?php

namespace App\Utiles\Hash;

use Illuminate\Support\Facades\Hash as LaravelHash;

/**
 * Class Hash
 * @package App\Utiles\Hash
 */
final class Hash implements HashInterface
{
    /**
     * @param string $string
     * @param array $options
     * @return string
     */
    public function make(string $string, array $options = [])
    {
        return LaravelHash::make($string, $options);
    }

    /**
     * @param string $string
     * @param string $hashedString
     * @param array $options
     * @return bool
     */
    public function check(string $string, string $hashedString, array $options = []): bool
    {
        return LaravelHash::check($string, $hashedString, $options);
    }
}

<?php

namespace App\Utiles\Fractal\Response;

use Spatie\Fractalistic\Fractal;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use App\Utiles\Fractal\Paginator\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Utiles\Fractal\Transformers\Transformer;
use App\Utiles\Fractal\Fractal\FractalInterface;
use App\Utiles\Fractal\Paginator\PaginatorInterface;
use App\Utiles\Fractal\Serializers\SerializerInterface;

/**
 * Class FractalResponse
 * @package App\Utiles\Fractal\Response
 */
final class FractalResponse implements FractalResponseInterface
{
    /**
     * @var FractalInterface
     */
    private $fractal;

    /**
     * @var Paginator
     */
    private $paginator;

    /**
     * @var array
     */
    private $additionalConfigs = [
        'includes' => 'parseIncludes',
        'excludes' => 'parseExcludes',
        'meta' => 'addMeta',
    ];

    /**
     * FractalResponse constructor.
     * @param FractalInterface $fractal
     * @param PaginatorInterface $paginator
     */
    public function __construct(FractalInterface $fractal, PaginatorInterface $paginator)
    {
        $this->fractal = $fractal;
        $this->paginator = $paginator;
    }

    /**
     * @param array|Model|Collection|LengthAwarePaginator $data
     * @param Transformer $transformer
     * @param SerializerInterface $serializer
     * @param array $options
     * @param bool $includeLinks
     * @return array
     */
    public function response(
        $data,
        Transformer $transformer,
        SerializerInterface $serializer,
        array $options = [],
        bool $includeLinks = true
    ): array {
        $fractal = $this->fractal->create($data, $transformer, $serializer, $includeLinks);
        if ($data instanceof LengthAwarePaginator) {
            $fractal = $this->paginateResponse($fractal, $data);
        }
        $fractal = $this->additionalConfiguration($fractal, $options);

        return $fractal->toArray();
    }

    /**
     * @param Fractal $fractal
     * @param LengthAwarePaginator $paginator
     * @return Fractal
     */
    private function paginateResponse(Fractal $fractal, LengthAwarePaginator $paginator)
    {
        $this->paginator->setPaginator($paginator);

        return $fractal->paginateWith($this->paginator);
    }

    /**
     * @param Fractal $fractal
     * @param array $options
     * @return Fractal
     */
    private function additionalConfiguration(Fractal $fractal, array $options = []) {
        foreach ($options as $key => $value) {
            $fractal->{$this->additionalConfigs[$key]}($value);
        }

        return $fractal;
    }
}

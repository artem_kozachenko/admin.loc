<?php

namespace App\Utiles\Fractal\Response;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use App\Utiles\Fractal\Transformers\Transformer;
use App\Utiles\Fractal\Serializers\SerializerInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Interface FractalResponseInterface
 * @package App\Utiles\Fractal\Response
 */
interface FractalResponseInterface
{
    /**
     * @param array|Model|Collection|LengthAwarePaginator $data
     * @param Transformer $transformer
     * @param SerializerInterface $serializer
     * @param array $options
     * @param bool $includeLinks
     * @return array
     */
    public function response(
        $data,
        Transformer $transformer,
        SerializerInterface $serializer,
        array $options = [],
        bool $includeLinks = true
    ): array;
}

<?php

namespace App\Utiles\Fractal\Serializers;

use League\Fractal\Serializer\Serializer as FractalSerializer;

/**
 * Interface JsonApiSerializerInterface
 * @package App\Utiles\Fractal\Serializers
 */
interface SerializerInterface extends FractalSerializer
{
    /**
     * @param string $baseUrl
     */
    public function setBaseUrl(string $baseUrl): void;
}

<?php

namespace App\Utiles\Fractal\Serializers\JsonApiSerializer;

use App\Utiles\Fractal\Serializers\SerializerInterface;

/**
 * Interface JsonApiSerializerInterface
 * @package App\Utiles\Fractal\Serializers\JsonApiSerializer
 */
interface JsonApiSerializerInterface extends SerializerInterface
{

}

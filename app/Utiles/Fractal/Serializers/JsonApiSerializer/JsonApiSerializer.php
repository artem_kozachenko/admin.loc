<?php

namespace App\Utiles\Fractal\Serializers\JsonApiSerializer;

use League\Fractal\Serializer\JsonApiSerializer as FractalJsonApiSerializer;

/**
 * Class JsonApiSerializer
 * @package App\Utiles\Fractal\Serializers\JsonApiSerializer
 */
final class JsonApiSerializer extends FractalJsonApiSerializer implements JsonApiSerializerInterface
{
    /**
     * @param string $baseUrl
     */
    public function setBaseUrl(string $baseUrl): void
    {
        $this->baseUrl = $baseUrl;
    }
}

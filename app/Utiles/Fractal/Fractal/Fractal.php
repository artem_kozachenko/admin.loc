<?php

namespace App\Utiles\Fractal\Fractal;

use App\Utiles\Validator\Factory;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Spatie\Fractal\Fractal as LaravelFractal;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Utiles\Fractal\Transformers\Transformer;
use Spatie\Fractalistic\Fractal as Fractalistic;
use App\Utiles\Fractal\Exception\FractalException;
use App\Utiles\ConfigUploader\ConfigUploaderInterface;
use App\Utiles\Fractal\Serializers\SerializerInterface;

/**
 * Class Fractal
 * @package App\Utiles\Fractal\Helper
 */
final class Fractal implements FractalInterface
{
    /**
     * @var string
     */
    private $baseUrl;

    /**
     * @var Factory
     */
    private $validator;

    /**
     * Fractal constructor.
     * @param Factory $factory
     * @param ConfigUploaderInterface $configUploader
     */
    public function __construct(Factory $factory, ConfigUploaderInterface $configUploader)
    {
        $this->validator = $factory;
        $this->configure($configUploader);
    }

    /**
     * @param ConfigUploaderInterface $configUploader
     */
    private function configure(ConfigUploaderInterface $configUploader)
    {
        $this->baseUrl = $configUploader->upload('fractal.base_url');
    }

    /**
     * @param array|Model|LengthAwarePaginator|Collection $data
     * @param Transformer $transformer
     * @param SerializerInterface $serializer
     * @param bool $includeLinks
     * @return Fractalistic
     * @throws \App\Utiles\Validator\Exceptions\ValidatorException
     * @throws FractalException
     */
    public function create(
        $data,
        Transformer $transformer,
        SerializerInterface $serializer,
        bool $includeLinks
    ): Fractalistic {
        if ($includeLinks) {
            $serializer->setBaseUrl($this->baseUrl);
        }
        if (! $this->validator->getValidators('fractal')->validate($data)) {
            throw new FractalException('Wrong data type');
        }

        return LaravelFractal::create($data ,$transformer, $serializer)
            ->withResourceName($transformer->getResourceKey());
    }
}

<?php

namespace App\Utiles\Fractal\Fractal;

use Illuminate\Database\Eloquent\Model;
use App\Utiles\Fractal\Transformers\Transformer;
use Spatie\Fractalistic\Fractal as Fractalistic;
use App\Utiles\Fractal\Serializers\SerializerInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Interface FractalInterface
 * @package App\Utiles\Fractal\Fractal
 */
interface FractalInterface
{
    /**
     * @param array|Model|LengthAwarePaginator $data
     * @param Transformer $transformer
     * @param SerializerInterface $serializer
     * @param bool $includeLinks
     * @return Fractalistic
     */
    public function create(
        $data,
        Transformer $transformer,
        SerializerInterface $serializer,
        bool $includeLinks
    ): Fractalistic;
}

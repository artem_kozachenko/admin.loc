<?php

namespace App\Utiles\Fractal\Paginator;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use League\Fractal\Pagination\PaginatorInterface as LeagueFractalPaginatorInterface;

/**
 * Interface PaginatorInterface
 * @package App\Utiles\Fractal\Paginator
 */
interface PaginatorInterface extends LeagueFractalPaginatorInterface
{
    /**
     * @return LengthAwarePaginator
     */
    public function getPaginator(): LengthAwarePaginator;

    /**
     * @param LengthAwarePaginator $paginator
     * @return void
     */
    public function setPaginator(LengthAwarePaginator $paginator): void;
}

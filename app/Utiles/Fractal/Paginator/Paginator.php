<?php

namespace App\Utiles\Fractal\Paginator;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Class Paginator
 * @package App\Utiles\Fractal\Paginator
 */
final class Paginator implements PaginatorInterface
{
    /**
     * The paginator instance.
     *
     * @var LengthAwarePaginator
     */
    protected $paginator;

    /**
     * Get the paginator instance.
     *
     * @return LengthAwarePaginator
     */
    public function getPaginator(): LengthAwarePaginator
    {
        return $this->paginator;
    }

    /**
     * Set the paginator instance.
     *
     * @param LengthAwarePaginator $paginator
     */
    public function setPaginator(LengthAwarePaginator $paginator): void
    {
        $this->paginator = $paginator;
    }

    /**
     * Get the current page.
     *
     * @return int
     */
    public function getCurrentPage()
    {
        return $this->paginator->currentPage();
    }

    /**
     * Get the last page.
     *
     * @return int
     */
    public function getLastPage()
    {
        return $this->paginator->lastPage();
    }

    /**
     * Get the total.
     *
     * @return int
     */
    public function getTotal()
    {
        return $this->paginator->total();
    }

    /**
     * Get the count.
     *
     * @return int
     */
    public function getCount()
    {
        return $this->paginator->count();
    }

    /**
     * Get the number per page.
     *
     * @return int
     */
    public function getPerPage()
    {
        return $this->paginator->perPage();
    }

    /**
     * Get the url for the given page.
     *
     * @param int $page
     *
     * @return string
     */
    public function getUrl($page)
    {
        return $this->paginator->url($page);
    }
}

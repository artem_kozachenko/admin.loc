<?php

namespace App\Utiles\Fractal\Transformers;

use League\Fractal\TransformerAbstract as FractalTransformerAbstract;

/**
 * Class Transformer
 * @package App\Utiles\Fractal\Transformer
 */
abstract class Transformer extends FractalTransformerAbstract
{
    /**
     * @var string
     */
    protected $resourceKey = '';

    /**
     * @return string
     */
    public function getResourceKey(): string
    {
        return $this->resourceKey;
    }
}

<?php

namespace App\Utiles\Fractal\Transformers;

/**
 * Class ErrorTransformer
 * @package App\Utiles\Fractal\Transformers
 */
final class ErrorTransformer extends Transformer
{
    /**
     * @var array
     */
    private $response = [];

    /**
     * @var string
     */
    protected $resourceKey = 'errors';

    /**
     * @param array $error
     * @return array
     */
    public function transform(array $error)
    {
        return $this
            ->setId($error)
            ->setLinks($error)
            ->setStatus($error)
            ->setCode($error)
            ->setTitle($error)
            ->setDetail($error)
            ->setSource($error)
            ->getResponse();
    }

    /**
     * @param array $error
     * @return $this
     */
    private function setId(array $error)
    {
        if (isset($error['id'])) {
            $this->response['id'] = $error['id'];
        } else {
            $this->response['id'] = '';
        }

        return $this;
    }

    /**
     * @param array $error
     * @return $this
     */
    private function setLinks(array $error)
    {
        if (isset($error['about'])) {
            $this->response['links']['about'] = $error['about'];
        }

        return $this;
    }

    /**
     * @param array $error
     * @return $this
     */
    private function setStatus(array $error)
    {
        if (isset($error['status'])) {
            $this->response['status'] = $error['status'];
        }

        return $this;
    }

    /**
     * @param array $error
     * @return $this
     */
    private function setCode(array $error)
    {
        if (isset($error['code'])) {
            $this->response['code'] = $error['code'];
        }

        return $this;
    }

    /**
     * @param array $error
     * @return $this
     */
    private function setTitle(array $error)
    {
        if (isset($error['title'])) {
            $this->response['title'] = $error['title'];
        }

        return $this;
    }

    /**
     * @param array $error
     * @return $this
     */
    private function setDetail(array $error)
    {
        if (isset($error['detail'])) {
            $this->response['detail'] = $error['detail'];
        }

        return $this;
    }

    /**
     * @param array $error
     * @return $this
     */
    private function setSource(array $error)
    {
        if (isset($error['pointer'])) {
            $this->response['status']['pointer'] = $error['pointer'];
        }

        return $this;
    }

    /**
     * @return array
     */
    private function getResponse()
    {
        return $this->response;
    }
}

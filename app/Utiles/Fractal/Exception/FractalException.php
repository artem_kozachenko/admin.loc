<?php

namespace App\Utiles\Fractal\Exception;

use Exception;

/**
 * Class FractalException
 * @package App\Utiles\Fractal\Exception
 */
final class FractalException extends Exception
{

}

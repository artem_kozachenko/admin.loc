<?php

namespace App\Utiles\Resolve;

/**
 * Interface ResolveHelperInterface
 * @package App\Utiles\Resolve
 */
interface ResolveHelperInterface
{
    /**
     * @param string $name
     * @return mixed
     */
    public function resolve(string $name);
}

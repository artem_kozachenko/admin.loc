<?php

namespace App\Utiles\Resolve;

/**
 * Class Resolve
 * @package App\Utiles\Resolve
 */
final class Resolve implements ResolveHelperInterface
{
    /**
     * @param string $name
     * @return mixed
     */
    public function resolve(string $name)
    {
        return resolve($name);
    }
}

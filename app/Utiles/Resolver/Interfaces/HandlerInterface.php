<?php

namespace App\Utiles\Resolver\Interfaces;

use App\Utiles\DTO\Interfaces\DTOInterface;

/**
 * Interface HandlerInterface
 * @package App\Utiles\Resolver\Interfaces
 */
interface HandlerInterface
{
    /**
     * @param DTOInterface $object
     * @return mixed
     */
    public function handle(DTOInterface $object);
}

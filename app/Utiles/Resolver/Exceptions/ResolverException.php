<?php

namespace App\Utiles\Resolver\Exceptions;

/**
 * Class ResolverException
 * @package App\Utiles\Resolver\Exceptions
 */
final class ResolverException extends \Exception
{

}

<?php

namespace App\Utiles\Resolver\Exceptions;

/**
 * Class HandlerException
 * @package App\Utiles\Resolver\Exceptions
 */
final class HandlerException extends \Exception
{

}

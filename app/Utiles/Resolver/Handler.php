<?php

namespace App\Utiles\Resolver;

use App\Utiles\DTO\Interfaces\DTOInterface;
use App\Utiles\Resolver\Exceptions\HandlerException;
use App\Utiles\Resolver\Interfaces\HandlerInterface;

/**
 * Class Handler
 * @package App\Utiles\Resolver
 */
abstract class Handler implements HandlerInterface
{
    /**
     * @param DTOInterface $object
     * @return mixed
     * @throws HandlerException
     */
    public function handle(DTOInterface $object)
    {
        if (! $this->isSatisfied($object)) {
            throw new HandlerException('%s cannot handle %s object', get_class($this), get_class($object));
        }

        return $this->proceed($object);
    }

    /**
     * @param $object
     * @return mixed
     */
    abstract protected function proceed($object);

    /**
     * @param DTOInterface $object
     * @return bool
     */
    abstract protected function isSatisfied(DTOInterface $object): bool;
}

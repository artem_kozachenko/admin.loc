<?php

namespace App\Utiles\Resolver;

use App\Utiles\ConfigUploader\ConfigUploader;
use App\Utiles\DTO\Interfaces\DTOInterface;
use App\Utiles\Resolver\Exceptions\ResolverException;
use App\Utiles\Resolver\Interfaces\HandlerInterface;

/**
 * Class Builder
 * @package App\Utiles\Resolver
 */
class Builder
{
    /**
     * @var Resolver
     */
    private $resolver;

    /**
     * @var array
     */
    private $schema = [];

    /**
     * Factory constructor.
     * @param Resolver $resolver
     * @param ConfigUploader $configUploader
     */
    public function __construct(Resolver $resolver, ConfigUploader $configUploader)
    {
        $this->resolver = $resolver;
        $this->configure($configUploader);
    }

    /**
     * @param ConfigUploader $configUploader
     */
    private function configure(ConfigUploader $configUploader)
    {
        $this->schema = $configUploader->upload('resolver.schema.schema');
    }

    /**
     * @param DTOInterface $searchObject
     * @return HandlerInterface
     * @throws ResolverException
     */
    public function buildHandler(DTOInterface $searchObject): HandlerInterface
    {
        $objectClassName = $this->identifyObject($searchObject);

        if (! $this->isSatisfy($objectClassName)) {
            throw new ResolverException(
                sprintf('Cannot build handler for given object - %s', $objectClassName)
            );
        }

        return $this->resolver->resolve($this->schema[$objectClassName]);
    }

    /**
     * @param string $object
     * @return bool
     */
    private function isSatisfy(string $object)
    {
        return isset($this->schema[$object]);
    }

    /**
     * @param DTOInterface $searchObject
     * @return string
     */
    private function identifyObject(DTOInterface $searchObject)
    {
        return get_class($searchObject);
    }
}

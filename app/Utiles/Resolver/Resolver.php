<?php

namespace App\Utiles\Resolver;

/**
 * Class Resolver
 * @package App\Utiles\Resolver
 */
class Resolver
{
    /**
     * @param string $className
     * @return mixed
     */
    public function resolve(string $className)
    {
        return resolve($className);
    }
}

<?php

namespace App\Utiles\Resolver;

use App\Utiles\DTO\Interfaces\DTOInterface;

/**
 * Class Dispatcher
 * @package App\Utiles\Resolver
 */
class Dispatcher
{
    /**
     * @var Builder
     */
    private $resolver;

    /**
     * TyreService constructor.
     * @param Builder $builder
     */
    public function __construct(Builder $builder)
    {
        $this->resolver = $builder;
    }

    /**
     * @param DTOInterface $object
     * @return mixed
     * @throws Exceptions\ResolverException
     */
    public function dispatch(DTOInterface $object)
    {
        $handler = $this->resolver->buildHandler($object);

        return $handler->handle($object);
    }
}

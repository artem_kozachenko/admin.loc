<?php

namespace App\Utiles\EntityManager;

use App\Utiles\EntityManager\Exceptions\EntityManagerException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use phpDocumentor\Reflection\Types\This;

/**
 * Class EntityManager
 * @package App\Utiles\EntityManager
 */
abstract class EntityManager implements EntityManagerInterface
{
    /**
     * @var string
     */
    protected $className;

    /**
     * @var array
     */
    protected $morphRelations = [];

    /**
     * Repository constructor.
     */
    public function __construct()
    {
        $this->className = $this->getClass();
    }

    /**
     * @return mixed
     */
    abstract protected function getClass();

    /**
     * Returns single entity from DB by its id
     *
     * @param int $id
     * @return Model
     */
    public function find(int $id)
    {
        return $this->className::find($id);
    }

    /**
     * Creates new entity with given params
     *
     * @param array $params
     * @return Model
     */
    public function create(array $params)
    {
        return $this->className::create($params);
    }

    /**
     * Updates given entity with given params
     *
     * @param Model $model
     * @param array $params
     * @return Model
     */
    public function update(Model $model, array $params)
    {
        $model->update($params);

        return $model;
    }

    /**
     * Saves given entity into DB
     *
     * @param Model $model
     * @return mixed
     */
    public function save(Model $model)
    {
        return $model->save();
    }

    /**
     * Updates entity by given id
     *
     * @param int $id
     * @param array $params
     * @return Model
     * @throws EntityManagerException
     */
    public function updateById(int $id, array $params)
    {
        $model = $this->find($id);
        if (! $model) {
            throw new EntityManagerException($this->generateExceptionMessage($id));
        }

        return $this->update($model, $params);
    }

    /**
     * Deletes Entity from DB
     *
     * @param Model $model
     * @return bool|null
     * @throws \Exception
     */
    public function delete(Model $model)
    {
        return empty($this->morphRelations) ? $this->destroy($model) : $this->deleteWithMorphes($model);
    }

    /**
     * Destroy entity from DB with its morph related entities.
     *
     * @param $model
     * @return bool|null
     * @throws \Exception
     */
    protected function deleteWithMorphes($model)
    {
        foreach ($this->morphRelations as $relation) {
            ($model->{$relation})
                ? $this->deleteMorph($model->{$relation})
                : null;
        }

        return $this->destroy($model);
    }

    /**
     * Delete given morph from DB
     *
     * @param $morph
     * @return bool|null
     * @throws \Exception
     */
    protected function deleteMorph($morph)
    {
        return $morph instanceof Model
            ? $this->destroy($morph)
            : $this->destroyCollection($morph);
    }

    /**
     * Remove all entities in given collection from DB
     *
     * @param Collection $models
     * @return mixed|void
     * @throws \Exception
     */
    public function deleteSeveral(Collection $models)
    {
        foreach ($models as $model) {
            $this->delete($model);
        }
    }

    /**
     * Delete collection of entities from DB
     *
     * @param Collection $collection
     * @throws \Exception
     */
    protected function destroyCollection(Collection $collection)
    {
        foreach ($collection as $model) {
            $this->destroy($model);
        }
    }

    /**
     * Function, that remove given model from DB
     *
     * @param Model $model
     * @return bool|null
     * @throws \Exception
     */
    protected function destroy(Model $model)
    {
        return $model->delete();
    }

    /**
     * Deletes entity by given id
     *
     * @param int $id
     * @return boolean
     * @throws EntityManagerException
     * @throws \Exception
     */
    public function deleteById(int $id)
    {
        $model = $this->find($id);
        if (! $model) {
            throw new EntityManagerException($this->generateExceptionMessage($id));
        }

        return $this->delete($model);
    }

    /**
     * Generates novel clean entity
     *
     * @return Model
     */
    public function getNovel()
    {
        return new $this->className();
    }

    /**
     * Store entity to the DB. If id is not given try to create new entity
     * with given params, otherwise try to update entity with given id.
     *
     * @param array $params
     * @param int|null $id
     * @return Model|mixed
     * @throws EntityManagerException
     */
    public function store(array $params, int $id = null)
    {
        return $id
            ? $this->updateById($id, $params)
            : $this->create($params);
    }

    /**
     * Generates exception message
     *
     * @param int $id
     * @return string
     */
    private function generateExceptionMessage(int $id)
    {
        return sprintf('Cannot find %s with given id(%d)', $this->className, $id);
    }

    /**
     * @param Model $model
     * @param array $ids
     * @param string $relation
     * @return Model
     */
    public function sync(Model $model, array $ids, string $relation)
    {
        $model->{$relation}()->sync($ids);

        return $model;
    }

    /**
     * @param Model $model
     * @param array $ids
     * @param string $relation
     * @return Model
     */
    public function attach(Model $model, array $ids, string $relation)
    {
        $model->{$relation}()->attach($ids);

        return $model;
    }

    /**
     * @param Model $model
     * @param array $ids
     * @param string $relation
     * @return Model
     */
    public function detach(Model $model, array $ids, string $relation)
    {
        $model->{$relation}()->detach($ids);

        return $model;
    }

    /**
     * @param Model $model
     * @param array $relations
     * @return Model
     */
    public function load(Model $model, array $relations = [])
    {
        $model->load($relations);

        return $model;
    }

    /**
     * @return mixed
     */
    public function deleteAll()
    {
        return $this->className::truncate();
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function whereDelete(array $params)
    {
        return $this->className::where($params)->delete();
    }
}

<?php

namespace App\Utiles\EntityManager\Exceptions;

/**
 * Class EntityManagerException
 * @package App\Utiles\EntityManager\Exceptions
 */
class EntityManagerException extends \Exception
{

}

<?php

namespace App\Utiles\EntityManager;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Interface EntityManagerInterface
 * @package App\Utiles\EntityManager
 */
interface EntityManagerInterface
{
    /**
     * Returns single entity from DB by its id
     *
     * @param int $id
     * @return Model
     */
    public function find(int $id);

    /**
     * Deletes Entity from DB
     *
     * @param Model $model
     * @return boolean
     */
    public function delete(Model $model);

    /**
     * Creates new entity with given params
     *
     * @param array $params
     * @return Model
     */
    public function create(array $params);

    /**
     * Updates given entity with given params
     *
     * @param Model $model
     * @param array $params
     * @return Model
     */
    public function update(Model $model, array $params);

    /**
     * Saves given entity into DB
     *
     * @param Model $model
     * @return mixed
     */
    public function save(Model $model);

    /**
     * Updates entity with given params by ID
     *
     * @param int $id
     * @param array $params
     * @return Model
    */
    public function updateById(int $id, array $params);

    /**
     * Deletes entity by given id
     *
     * @param int $id
     * @return boolean
     */
    public function deleteById(int $id);

    /**
     * Creates new clean entity
     *
     * @return mixed
     */
    public function getNovel();

    /**
     * Store entity to the DB. If id is not given try to create new entity
     * with given params, otherwise try to update entity with given id.
     *
     * @param array $params
     * @param int|null $id
     * @return mixed
     */
    public function store(array $params, int $id = null);

    /**
     * Remove all entities in given collection from DB
     *
     * @param Collection $models
     * @return mixed
     */
    public function deleteSeveral(Collection $models);

    /**
     * @param Model $model
     * @param array $ids
     * @param string $relation
     * @return Model
     */
    public function sync(Model $model, array $ids, string $relation);

    /**
     * @param Model $model
     * @param array $ids
     * @param string $relation
     * @return Model
     */
    public function attach(Model $model, array $ids, string $relation);

    /**
     * @param Model $model
     * @param array $ids
     * @param string $relation
     * @return Model
     */
    public function detach(Model $model, array $ids, string $relation);

    /**
     * @param Model $model
     * @param array $relations
     * @return mixed
     */
    public function load(Model $model, array $relations = []);

    /**
     * @return mixed
     */
    public function deleteAll();

    /**
     * @param array $params
     * @return mixed
     */
    public function whereDelete(array $params);
}

<?php

namespace App\Http\Controllers\Admin;

use App\Commands\User\User\CreateUserCommand;
use App\Commands\User\User\DeleteUserCommand;
use App\Commands\User\User\GetUsersCommand;
use App\Commands\User\User\UpdateUserCommand;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\UsersCollection;
use App\Services\User\User\FractalTransformer\FractalUserTransformer;
use App\Utiles\Fractal\Response\FractalResponseInterface;
use App\Utiles\Fractal\Serializers\JsonApiSerializer\JsonApiSerializerInterface;
use App\Utiles\Resolver\Dispatcher;
use Illuminate\Http\Request;

/**
 * Class UserController
 * @package App\Http\Controllers\Admin
 */
final class UserController extends Controller
{
    /**
     * @var Dispatcher
     */
    private $dispatcher;

    /**
     * @var FractalResponseInterface
     */
    private $fractalResponse;

    /**
     * @var FractalUserTransformer
     */
    private $userTransformer;

    /**
     * @var JsonApiSerializerInterface
     */
    private $jsonApiSerializer;

    /**
     * UserController constructor.
     * @param Dispatcher $dispatcher
     * @param FractalResponseInterface $fractalResponse
     * @param FractalUserTransformer $userTransformer
     * @param JsonApiSerializerInterface $jsonApiSerializer
     */
    public function __construct(
        Dispatcher $dispatcher,
        FractalResponseInterface $fractalResponse,
        FractalUserTransformer $userTransformer,
        JsonApiSerializerInterface $jsonApiSerializer
    ) {
        $this->dispatcher = $dispatcher;
        $this->fractalResponse = $fractalResponse;
        $this->userTransformer = $userTransformer;
        $this->jsonApiSerializer = $jsonApiSerializer;
    }

    /**
     * @param Request $request
     * @return UsersCollection
     * @throws \App\Utiles\Resolver\Exceptions\ResolverException
     */
    public function index(Request $request)
    {
        $users = $this->dispatcher->dispatch(new GetUsersCommand($request->input('per_page')));
        $response = $this->fractalResponse->response($users, $this->userTransformer, $this->jsonApiSerializer);

        return response()->json($response);
    }

    /**
     * @param CreateUserRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Utiles\Resolver\Exceptions\ResolverException
     */
    public function store(CreateUserRequest $request)
    {
        $user = $this->dispatcher->dispatch(new CreateUserCommand($request->input()));
        $response = $this->fractalResponse->response($user, $this->userTransformer, $this->jsonApiSerializer);

        return response()->json($response);
    }

    /**
     * @param UpdateUserRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Utiles\Resolver\Exceptions\ResolverException
     */
    public function update(UpdateUserRequest $request, int $id)
    {
        $user = $this->dispatcher->dispatch(new UpdateUserCommand($request->input(), $id));
        $response = $this->fractalResponse->response($user, $this->userTransformer, $this->jsonApiSerializer);

        return response()->json($response);
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Utiles\Resolver\Exceptions\ResolverException
     */
    public function delete(int $id)
    {
        $response = $this->dispatcher->dispatch(new DeleteUserCommand($id));
        // TODO build response
        return response()->json($response);
    }
}

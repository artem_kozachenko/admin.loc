<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Commands\User\Auth\RegisterCommand;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Utiles\Resolver\Dispatcher;

/**
 * Class RegisterController
 * @package App\Http\Controllers\Admin\Auth
 */
final class RegisterController extends Controller
{
    /**
     * @var Dispatcher
     */
    private $dispatcher;

    /**
     * RegisterController constructor.
     * @param Dispatcher $dispatcher
     */
    public function __construct(Dispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param RegisterRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Utiles\Resolver\Exceptions\ResolverException
     */
    public function register(RegisterRequest $request)
    {
        $token = $this->dispatcher->dispatch(new RegisterCommand($request->input()));

        return response()->json(['accessToken' => $token], 200);
    }
}

<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Commands\User\Auth\LoginCommand;
use App\Commands\User\Auth\LogoutCommand;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Utiles\Resolver\Dispatcher;
use Illuminate\Http\Request;

/**
 * Class LoginController
 * @package App\Http\Controllers\Admin\Auth
 */
final class LoginController extends Controller
{
    /**
     * @var Dispatcher
     */
    private $dispatcher;

    /**
     * LoginController constructor.
     * @param Dispatcher $dispatcher
     */
    public function __construct(Dispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param LoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Utiles\Resolver\Exceptions\ResolverException
     */
    public function login(LoginRequest $request)
    {
        $response = $this->dispatcher->dispatch(new LoginCommand($request->input()));

        return response()->json($response['payload'], $response['code']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Utiles\Resolver\Exceptions\ResolverException
     */
    public function logout(Request $request)
    {
        $this->dispatcher->dispatch(new LogoutCommand($request->input(), $request->bearerToken()));

        return response()->json(['message' => 'You\'ve been successfully logged out!'], 200);
    }
}

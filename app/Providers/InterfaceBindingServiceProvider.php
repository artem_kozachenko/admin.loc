<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class InterfaceBindingServiceProvider
 * @package App\Providers
 */
final class InterfaceBindingServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $bindings = $this->configure();

        $this->bind($bindings);
    }

    /**
     * @return array|null
     */
    protected function configure()
    {
        return config('bindings');
    }

    /**
     * @param array $bindingTypes
     */
    protected function bind(array $bindingTypes)
    {
        foreach ($bindingTypes as $bindingType) {
            $this->bindByType($bindingType);
        }
    }

    /**
     * @param array $bindingType
     */
    protected function bindByType(array $bindingType)
    {
        foreach ($bindingType as $interface => $implementation) {
            $this->app->bind($interface, $implementation);
        }
    }
}

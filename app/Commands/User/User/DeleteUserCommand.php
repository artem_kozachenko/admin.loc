<?php

namespace App\Commands\User\User;

use App\Utiles\DTO\Interfaces\DTOInterface;

/**
 * Class DeleteUserCommand
 * @package App\Commands\User\User
 */
final class DeleteUserCommand implements DTOInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * DeleteUserCommand constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return ['id' => $this->id];
    }
}

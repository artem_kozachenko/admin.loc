<?php

namespace App\Commands\User\User;

use App\Utiles\DTO\Interfaces\DTOInterface;

/**
 * Class CreateUserCommand
 * @package App\Commands\User\User
 */
final class CreateUserCommand implements DTOInterface
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $password;

    /**
     * CreateUserCommand constructor.
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->name = $params['name'];
        $this->email = $params['email'];
        $this->password = $params['password'];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'name' => $this->name,
            'email' => $this->email,
            'password' => $this->password,
        ];
    }
}

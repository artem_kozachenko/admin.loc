<?php

namespace App\Commands\User\User;

use App\Utiles\DTO\Interfaces\DTOInterface;

/**
 * Class GetAllUsersCommand
 * @package App\Commands\User\User
 */
final class GetUsersCommand implements DTOInterface
{
    /**
     * @var int|null
     */
    private $perPage;

    /**
     * GetUsersCommand constructor.
     * @param int|null $perPage
     */
    public function __construct(int $perPage = null)
    {
        $this->perPage = $perPage;
    }

    /**
     * @return int|null
     */
    public function getPerPage(): ?int
    {
        return $this->perPage;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return ['perPage' => $this->perPage];
    }
}

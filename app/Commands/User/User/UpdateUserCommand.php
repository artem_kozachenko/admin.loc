<?php

namespace App\Commands\User\User;

use App\Utiles\DTO\Interfaces\DTOInterface;

/**
 * Class UpdateUserCommand
 * @package App\Commands\User\User
 */
final class UpdateUserCommand implements DTOInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $password;

    /**
     * UpdateUserCommand constructor.
     * @param array $params
     * @param int $id
     */
    public function __construct(array $params, int $id)
    {
        $this->id = $id;
        $this->name = $params['name'];
        $this->email = $params['email'];
        $this->password = $params['password'];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'password' => $this->password,
        ];
    }
}

<?php

namespace App\Commands\User\Auth;

use App\Utiles\DTO\Interfaces\DTOInterface;

/**
 * Class LogoutCommand
 * @package App\Commands\User\Auth
 */
final class LogoutCommand implements DTOInterface
{
    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $bearerToken;

    /**
     * LogoutCommand constructor.
     * @param array $params
     * @param string $bearerToken
     */
    public function __construct(array $params, string $bearerToken)
    {
        $this->email = $params['email'];
        $this->bearerToken = $bearerToken;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getBearerToken(): string
    {
        return $this->bearerToken;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [];
    }
}

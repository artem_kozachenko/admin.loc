<?php

namespace App\Commands\User\Auth;

use App\Services\User\User\DB\Model\User;
use App\Utiles\DTO\Interfaces\DTOInterface;

/**
 * Class RegisterCommand
 * @package App\Commands\User\User
 */
final class RegisterCommand implements DTOInterface
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $password;

    /**
     * RegisterCommand constructor.
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->name = $params['name'];
        $this->email = $params['email'];
        $this->password = $params['password'];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            User::FIELD_NAME => $this->name,
            User::FIELD_EMAIL => $this->email,
            User::FIELD_PASSWORD => $this->password,
        ];
    }
}

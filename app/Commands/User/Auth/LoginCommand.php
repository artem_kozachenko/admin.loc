<?php

namespace App\Commands\User\Auth;

use App\Utiles\DTO\Interfaces\DTOInterface;

/**
 * Class LoginCommand
 * @package App\Commands\User\Auth
 */
final class LoginCommand implements DTOInterface
{
    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $password;

    /**
     * LoginCommand constructor.
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->email = $params['email'];
        $this->password = $params['password'];
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [];
    }
}

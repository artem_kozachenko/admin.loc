<?php

namespace App\Services\Shared\Image;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Image
 * @package App\Services\Shared\Image
 */
final class Image extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function imageable()
    {
        return $this->morphTo();
    }
}

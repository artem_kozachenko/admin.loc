<?php

namespace App\Services\Shared\Image\FractalTransformer;

use App\Services\Shared\Image\Image;
use App\Utiles\Fractal\Transformers\Transformer;

/**
 * Class FractalImageTransformer
 * @package App\Services\Shared\Image\FractalTransformer
 */
final class FractalImageTransformer extends Transformer
{
    /**
     * @return string
     */
    protected function setResourceKey(): string
    {
        return 'image';
    }

    /**
     * @param Image $image
     * @return array
     */
    public function transform(Image $image = null)
    {
        return $image
            ? [
                'id' => $image->id,
                'path' => $image->path
            ]
            : ['id' => null];
    }
}

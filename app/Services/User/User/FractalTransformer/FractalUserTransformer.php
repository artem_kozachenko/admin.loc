<?php

namespace App\Services\User\User\FractalTransformer;

use App\Services\Shared\Image\FractalTransformer\FractalImageTransformer;
use App\Services\User\User\DB\Model\User;
use App\Utiles\Fractal\Transformers\Transformer;

/**
 * Class FractalUserTransformer
 * @package App\Services\User\User\FractalTransformer
 */
final class FractalUserTransformer extends Transformer
{
    /**
     * @var string
     */
    protected $resourceKey = 'users';

    /**
     * @var FractalImageTransformer
     */
    private $imageTransformer;

    /**
     * FractalUserTransformer constructor.
     * @param FractalImageTransformer $imageTransformer
     */
    public function __construct(FractalImageTransformer $imageTransformer)
    {
        $this->imageTransformer = $imageTransformer;
    }

    /**
     * @param User $user
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email
        ];
    }

    /**
     * @param User $user
     * @return \League\Fractal\Resource\Item
     */
    public function includeImage(User $user)
    {
        return $this->item($user->image, $this->imageTransformer, $this->imageTransformer->getResourceKey());
    }
}

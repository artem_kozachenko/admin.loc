<?php

namespace App\Services\User\User\Handlers;

use App\Commands\User\User\CreateUserCommand;
use App\Services\User\User\DB\Manager\ManagerInterface;
use App\Utiles\DTO\Interfaces\DTOInterface;
use App\Utiles\Hash\HashInterface;
use App\Utiles\Resolver\Handler;

/**
 * Class CreateUserHandler
 * @package App\Services\User\User\Handlers
 */
final class CreateUserHandler extends Handler
{
    /**
     * @var HashInterface
     */
    private $hash;

    /**
     * @var ManagerInterface
     */
    private $userManager;

    /**
     * CreateUserHandler constructor.
     * @param HashInterface $hash
     * @param ManagerInterface $userManager
     */
    public function __construct(HashInterface $hash, ManagerInterface $userManager)
    {
        $this->hash = $hash;
        $this->userManager = $userManager;
    }

    /**
     * @param $object
     * @return \Illuminate\Database\Eloquent\Model|mixed
     */
    protected function proceed($object)
    {
        $object = $this->hashPassword($object);

        return $this->createUser($object->toArray());
    }

    /**
     * @param CreateUserCommand $object
     * @return CreateUserCommand
     */
    private function hashPassword(CreateUserCommand $object)
    {
        $password = $object->getPassword();
        $hashedPassword = $this->hash->make($password);
        $object->setPassword($hashedPassword);

        return $object;
    }

    /**
     * @param array $params
     * @return \Illuminate\Database\Eloquent\Model
     */
    private function createUser(array $params)
    {
        return $this->userManager->create($params);
    }

    /**
     * @param DTOInterface $object
     * @return bool
     */
    protected function isSatisfied(DTOInterface $object): bool
    {
        return $object instanceof CreateUserCommand;
    }
}

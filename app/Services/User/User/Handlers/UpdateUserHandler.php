<?php

namespace App\Services\User\User\Handlers;

use App\Commands\User\User\UpdateUserCommand;
use App\Services\User\User\DB\Model\User;
use App\Services\User\User\DB\Repository\RepositoryInterface;
use App\Utiles\DTO\Interfaces\DTOInterface;
use App\Utiles\Hash\HashInterface;
use App\Utiles\Resolver\Handler;
use App\Services\User\User\DB\Manager\ManagerInterface;
use App\Utiles\Serializer\SerializerInterface;
use App\Services\User\User\DB\QueryObject\QueryObject as UsersQueryObject;

/**
 * Class UpdateUserHandler
 * @package App\Services\User\User\Handlers
 */
final class UpdateUserHandler extends Handler
{
    /**
     * @var HashInterface
     */
    private $hash;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var ManagerInterface
     */
    private $userManager;

    /**
     * @var RepositoryInterface
     */
    private $userRepository;

    /**
     * UpdateUserHandler constructor.
     * @param HashInterface $hash
     * @param ManagerInterface $userManager
     * @param SerializerInterface $serializer
     * @param RepositoryInterface $userRepository
     */
    public function __construct(
        HashInterface $hash,
        ManagerInterface $userManager,
        SerializerInterface $serializer,
        RepositoryInterface $userRepository
    ) {
        $this->hash = $hash;
        $this->serializer = $serializer;
        $this->userManager = $userManager;
        $this->userRepository = $userRepository;
    }

    /**
     * @param $object
     * @return mixed
     */
    protected function proceed($object)
    {
        /**
         * @var User $user
         */
        $object = $this->hashPassword($object);
        $user = $this->getUser($object->getId())->first();

        return $this->updateUser($user, $object->toArray());
    }

    /**
     * @param UpdateUserCommand $object
     * @return UpdateUserCommand
     */
    private function hashPassword(UpdateUserCommand $object)
    {
        $password = $object->getPassword();
        $hashedPassword = $this->hash->make($password);
        $object->setPassword($hashedPassword);

        return $object;
    }

    /**
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Collection
     */
    private function getUser(int $id)
    {
        $queryObject = $this->serializer->deserialize(['id' => $id], UsersQueryObject::class);

        return $this->userRepository->getByQueryObject($queryObject);
    }

    /**
     * @param User $user
     * @param array $params
     * @return \Illuminate\Database\Eloquent\Model
     */
    private function updateUser(User $user, array $params)
    {
        return $this->userManager->update($user, $params);
    }

    /**
     * @param DTOInterface $object
     * @return bool
     */
    protected function isSatisfied(DTOInterface $object): bool
    {
        return $object instanceof UpdateUserCommand;
    }
}

<?php

namespace App\Services\User\User\Handlers;

use App\Utiles\Resolver\Handler;
use App\Commands\User\User\GetUsersCommand;
use App\Utiles\DTO\Interfaces\DTOInterface;
use App\Utiles\Serializer\SerializerInterface;
use App\Services\User\User\DB\Repository\RepositoryInterface;
use App\Services\User\User\DB\QueryObject\QueryObject as UsersQueryObject;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class GetAllUsersHandler
 * @package App\Services\User\User\Handlers
 */
final class GetUsersHandler extends Handler
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var RepositoryInterface
     */
    private $userRepository;

    /**
     * GetUsersHandler constructor.
     * @param SerializerInterface $serializer
     * @param RepositoryInterface $userRepository
     */
    public function __construct(SerializerInterface $serializer, RepositoryInterface $userRepository)
    {
        $this->serializer = $serializer;
        $this->userRepository = $userRepository;
    }

    /**
     * @param GetUsersCommand $object
     * @return Collection|LengthAwarePaginator
     */
    protected function proceed($object)
    {
        $queryObject = $this->serializer->deserialize($object->toArray(), UsersQueryObject::class);

        return $this->userRepository->getByQueryObject($queryObject);
    }

    /**
     * @param DTOInterface $object
     * @return bool
     */
    protected function isSatisfied(DTOInterface $object): bool
    {
        return $object instanceof GetUsersCommand;
    }
}

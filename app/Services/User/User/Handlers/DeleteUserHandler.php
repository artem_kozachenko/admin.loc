<?php

namespace App\Services\User\User\Handlers;

use App\Commands\User\User\DeleteUserCommand;
use App\Utiles\DTO\Interfaces\DTOInterface;
use App\Utiles\Resolver\Handler;
use App\Services\User\User\DB\Manager\ManagerInterface;

/**
 * Class DeleteUserHandler
 * @package App\Services\User\User\Handlers
 */
final class DeleteUserHandler extends Handler
{
    /**
     * @var ManagerInterface
     */
    private $userManager;

    /**
     * CreateUserHandler constructor.
     * @param ManagerInterface $userManager
     */
    public function __construct(ManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @param DeleteUserCommand $object
     * @return bool
     */
    protected function proceed($object)
    {
        return $this->deleteUser($object->getId());
    }

    /**
     * @param int $id
     * @return bool
     */
    private function deleteUser(int $id): bool
    {
        return $this->userManager->deleteById($id);
    }

    /**
     * @param DTOInterface $object
     * @return bool
     */
    protected function isSatisfied(DTOInterface $object): bool
    {
        return $object instanceof DeleteUserCommand;
    }
}

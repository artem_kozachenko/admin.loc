<?php

namespace App\Services\User\User\DB\Repository;

use App\Services\User\User\DB\Model\User;
use App\Utiles\Repository\Repository as BaseRepository;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class Repository
 * @package App\Services\User\User\DB\Repository
 */
final class Repository extends BaseRepository implements RepositoryInterface
{
    /**
     * @return Builder
     */
    protected function setQueryBuilder(): Builder
    {
        return User::query();
    }
}

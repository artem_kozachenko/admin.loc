<?php

namespace App\Services\User\User\DB\Repository;

use App\Utiles\Repository\RepositoryInterface as BaseRepositoryInterface;

/**
 * Interface RepositoryInterface
 * @package App\Services\User\User\DB\Repository
 */
interface RepositoryInterface extends BaseRepositoryInterface
{

}

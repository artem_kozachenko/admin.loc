<?php

namespace App\Services\User\User\DB\Model;

/**
 * Interface UserInterface
 * @package App\Services\User\User\DB\Model
 */
interface UserInterface
{
    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param string $name
     * @return mixed
     */
    public function setName(string $name);

    /**
     * @return string
     */
    public function getEmail(): string;

    /**
     * @param string $email
     * @return mixed
     */
    public function setEmail(string $email);

    /**
     * @return string
     */
    public function getPassword(): string;

    /**
     * @param string $password
     * @return mixed
     */
    public function setPassword(string $password);

    /**
     * @return int
     */
    public function getRoleId(): int;

    /**
     * @param int $roleId
     * @return mixed
     */
    public function setRoleId(int $roleId);

    /**
     * @return mixed
     */
    public function getRole();
}

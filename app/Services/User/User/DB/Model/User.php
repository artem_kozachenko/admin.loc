<?php

namespace App\Services\User\User\DB\Model;

use App\Services\Shared\Image\Image;
use App\Services\User\Role\DB\Model\Role;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

/**
 * Class User
 * @package App\Services\User\User\DB\Model
 */
final class User extends Authenticatable implements UserInterface
{
    /*__________________________________________________________________________________________________________________
    |
    |                                              TRAITS
    |___________________________________________________________________________________________________________________
    */

    use Notifiable, HasApiTokens;

    /*__________________________________________________________________________________________________________________
    |
    |                                              CONSTANTS
    |___________________________________________________________________________________________________________________
    */

    /*
    |-------------------------------------------------------------------------------------------------------------------
    |                                                FIELDS
    |-------------------------------------------------------------------------------------------------------------------
    */

    const FIELD_NAME = 'name';

    const FIELD_EMAIL = 'email';

    const FIELD_PASSWORD = 'password';

    const FIELD_ROLE_ID = 'role_id';

    /*
    |-------------------------------------------------------------------------------------------------------------------
    |                                                RELATIONS
    |-------------------------------------------------------------------------------------------------------------------
    */

    const RELATION_ROLE = 'role';


    /*__________________________________________________________________________________________________________________
    |
    |                                              SETTINGS
    |___________________________________________________________________________________________________________________
    */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_NAME,
        self::FIELD_EMAIL,
        self::FIELD_PASSWORD,
        self::FIELD_ROLE_ID,
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        self::FIELD_PASSWORD,
        'remember_token',
    ];

    /*__________________________________________________________________________________________________________________
    |
    |                                              MAIN ATTRIBUTES
    |___________________________________________________________________________________________________________________
    */

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this|mixed
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this|mixed
     */
    public function setEmail(string $email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return $this|mixed
     */
    public function setPassword(string $password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return int
     */
    public function getRoleId(): int
    {
        return $this->tole_id;
    }

    /**
     * @param int $roleId
     * @return $this|mixed
     */
    public function setRoleId(int $roleId)
    {
        $this->role_id = $roleId;

        return $this;
    }

    /*__________________________________________________________________________________________________________________
    |
    |                                              RELATIONS
    |___________________________________________________________________________________________________________________
    */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }
}

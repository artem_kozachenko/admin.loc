<?php

namespace App\Services\User\User\DB\QueryObject;

use App\Utiles\Repository\QueryObject\QueryObject as BaseQueryObject;
use App\Utiles\Serializer\Interfaces\SerializableObjectInterface;

/**
 * Class QueryObject
 * @package App\Services\User\User\DB\QueryObject
 */
final class QueryObject extends BaseQueryObject implements SerializableObjectInterface
{
    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->addToWhere('id', $id);
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->addToWhere('name', $name);
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->addToWhere('email', $email);
    }
}

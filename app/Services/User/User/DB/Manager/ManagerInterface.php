<?php

namespace App\Services\User\User\DB\Manager;

use App\Utiles\EntityManager\EntityManagerInterface as BaseManagerInterface;

/**
 * Interface ManagerInterface
 * @package App\Services\User\User\DB\Manager
 */
interface ManagerInterface extends BaseManagerInterface
{

}

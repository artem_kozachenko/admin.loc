<?php

namespace App\Services\User\User\DB\Manager;

use App\Services\User\User\DB\Model\User;
use App\Utiles\EntityManager\EntityManager as BaseManager;

/**
 * Class Manager
 * @package App\Services\User\User\DB\Manager
 */
final class Manager extends BaseManager implements ManagerInterface
{
    /**
     * @return string
     */
    protected function getClass()
    {
        return User::class;
    }
}

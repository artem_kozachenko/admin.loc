<?php

namespace App\Services\User\Auth\Handlers;

use App\Commands\User\Auth\LogoutCommand;
use App\Services\User\User\DB\Model\User;
use App\Services\User\User\DB\Repository\RepositoryInterface;
use App\Utiles\DTO\Interfaces\DTOInterface;
use App\Utiles\Resolver\Handler;
use Lcobucci\JWT\Parser;

/**
 * Class LogoutHandler
 * @package App\Services\User\Auth\Handlers
 */
final class LogoutHandler extends Handler
{
    const USER_LIMIT = 1;

    /**
     * @var Parser
     */
    private $parser;

    /**
     * @var RepositoryInterface
     */
    private $userRepository;

    /**
     * LogoutHandler constructor.
     * @param Parser $parser
     * @param RepositoryInterface $userRepository
     */
    public function __construct(Parser $parser, RepositoryInterface $userRepository)
    {
        $this->parser = $parser;
        $this->userRepository = $userRepository;
    }

    /**
     * @param LogoutCommand $object
     */
    protected function proceed($object)
    {
        $tokenId = $this->getTokenId($object->getBearerToken());
        $user = $this->findUser($object->getEmail());
        $this->revokeToken($user, $tokenId);
    }

    /**
     * @param string $bearerToken
     * @return mixed
     */
    private function getTokenId(string $bearerToken)
    {
        return $this->parser->parse($bearerToken)->getHeader('jti');
    }

    /**
     * @param string $email
     * @return mixed
     */
    private function findUser(string $email)
    {
        return $this->userRepository->findBy(
            ['email' => $email],
            ['limit' => self::USER_LIMIT]
        )
            ->first();
    }

    /**
     * @param User $user
     * @param string $tokenId
     */
    private function revokeToken(User $user, string $tokenId)
    {
        $user->tokens->find($tokenId)->revoke();
    }

    /**
     * @param DTOInterface $object
     * @return bool
     */
    protected function isSatisfied(DTOInterface $object): bool
    {
        return $object instanceof LogoutCommand;
    }
}

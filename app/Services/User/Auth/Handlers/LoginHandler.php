<?php

namespace App\Services\User\Auth\Handlers;

use App\Commands\User\Auth\LoginCommand;
use App\Services\User\User\DB\Model\User;
use App\Services\User\User\DB\Repository\RepositoryInterface;
use App\Utiles\DTO\Interfaces\DTOInterface;
use App\Utiles\Hash\Hash;
use App\Utiles\Resolver\Handler;

/**
 * Class LoginHandler
 * @package App\Services\User\Auth\Handlers
 */
final class LoginHandler extends Handler
{
    const USER_LIMIT = 1;

    const TOKEN_NAME = 'Personal Access Token';

    const SUCCESS_CODE = 200;

    const FORBIDDEN_CODE = 401;

    /**
     * @var Hash
     */
    private $hash;

    /**
     * @var RepositoryInterface
     */
    private $userRepository;

    /**
     * LoginHandler constructor.
     * @param Hash $hash
     * @param RepositoryInterface $userRepository
     */
    public function __construct(Hash $hash, RepositoryInterface $userRepository)
    {
        $this->hash = $hash;
        $this->userRepository = $userRepository;
    }

    /**
     * @param LoginCommand $object
     * @return array|mixed
     */
    protected function proceed($object)
    {
        /**
         * @var User $user
         */
        $user = $this->findUser($object->getEmail());
        $passwordMatch = $this->hash->check($object->getPassword(), $user->getPassword());

        return $passwordMatch
            ? $this->success($user)
            : $this->error();
    }

    /**
     * @param string $email
     * @return mixed
     */
    private function findUser(string $email)
    {
        return $this->userRepository->findBy(
            ['email' => $email],
            ['limit', self::USER_LIMIT]
        )
            ->first();
    }

    /**
     * @param User $user
     * @return array
     */
    private function success(User $user)
    {
        $token = $this->createToken($user);

        return $this->makeResponse(['accessToken' => $token], self::SUCCESS_CODE);
    }

    /**
     * @param User $user
     * @return string
     */
    private function createToken(User $user)
    {
        return $user->createToken(self::TOKEN_NAME)->accessToken;
    }

    /**
     * @return array
     */
    private function error()
    {
        return $this->makeResponse(['error' => 'Wrong email or password!'], self::FORBIDDEN_CODE);
    }

    /**
     * @param array $responseData
     * @param int $code
     * @return array
     */
    private function makeResponse(array $responseData, int $code)
    {
        return [
            'payload' => $responseData,
            'code' => $code
        ];
    }

    /**
     * @param DTOInterface $object
     * @return bool
     */
    protected function isSatisfied(DTOInterface $object): bool
    {
        return $object instanceof LoginCommand;
    }
}

<?php

namespace App\Services\User\Auth\Handlers;

use App\Commands\User\Auth\RegisterCommand;
use App\Services\User\User\DB\Manager\ManagerInterface;
use App\Services\User\User\DB\Model\User;
use App\Services\User\User\DB\Repository\RepositoryInterface;
use App\Utiles\DTO\Interfaces\DTOInterface;
use App\Utiles\Hash\HashInterface;
use App\Utiles\Resolver\Handler;
use Illuminate\Database\Eloquent\Model;

/**
 * Class RegisterHandler
 * @package App\Services\User\User\Handlers
 */
final class RegisterHandler extends Handler
{
    const USER_LIMIT = 1;

    const TOKEN_NAME = 'Personal Access Token';

    /**
     * @var HashInterface
     */
    private $hash;

    /**
     * @var ManagerInterface
     */
    private $userManager;

    /**
     * @var RepositoryInterface
     */
    private $userRepository;

    /**
     * RegisterHandler constructor.
     * @param HashInterface $hash
     * @param ManagerInterface $userManager
     * @param RepositoryInterface $userRepository
     */
    public function __construct(
        HashInterface $hash,
        ManagerInterface $userManager,
        RepositoryInterface $userRepository
    ) {
        $this->hash = $hash;
        $this->userManager = $userManager;
        $this->userRepository = $userRepository;
    }

    /**
     * @param RegisterCommand $object
     * @return string
     */
    protected function proceed($object)
    {
        $object = $this->hashPassword($object);
        $user = $this->createUser($object->toArray());
        /**
         * @var User $user
         */
        $tokenResult = $user->createToken(self::TOKEN_NAME);

        return $tokenResult->accessToken;
    }

    /**
     * @param RegisterCommand $object
     * @return RegisterCommand
     */
    private function hashPassword(RegisterCommand $object)
    {
        $hashedPassword = $this->hash->make($object->getPassword());
        $object->setPassword($hashedPassword);

        return $object;
    }

    /**
     * @param array $params
     * @return Model
     */
    private function createUser(array $params)
    {
        return $this->userManager->create($params);
    }

    /**
     * @param DTOInterface $object
     * @return bool
     */
    protected function isSatisfied(DTOInterface $object): bool
    {
        return $object instanceof RegisterCommand;
    }
}

<?php

namespace App\Services\User\Role\DB\Repository;

use App\Services\User\Role\DB\Model\Role;
use App\Utiles\Repository\Repository as BaseRepository;
use App\Utiles\Serializer\Interfaces\SerializableObjectInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Repository
 * @package App\Services\User\Role\DB\Repository
 */
final class Repository extends BaseRepository implements RepositoryInterface, SerializableObjectInterface
{
    /**
     * @return Builder
     */
    protected function setQueryBuilder(): Builder
    {
        return Role::query();
    }

    /**
     * @return Model
     */
    public function getSuperAdmin(): Model
    {
        return Role::where(Role::FIELD_NAME, Role::VALUE_SUPER_ADMIN)->first();
    }

    /**
     * @return Model
     */
    public function getAdmin(): Model
    {
        return Role::where(Role::FIELD_NAME, Role::VALUE_ADMIN)->first();
    }

    /**
     * @return Model
     */
    public function getEditor(): Model
    {
        return Role::where(Role::FIELD_NAME, Role::VALUE_EDITOR)->first();
    }
}

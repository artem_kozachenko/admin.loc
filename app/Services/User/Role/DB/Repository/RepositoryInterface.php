<?php

namespace App\Services\User\Role\DB\Repository;

use App\Utiles\Repository\RepositoryInterface as BaseRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Interface RepositoryInterface
 * @package App\Services\User\Role\DB\Repository
 */
interface RepositoryInterface extends BaseRepositoryInterface
{
    /**
     * @return Model
     */
    public function getSuperAdmin(): Model;

    /**
     * @return Model
     */
    public function getAdmin(): Model;

    /**
     * @return Model
     */
    public function getEditor(): Model;
}

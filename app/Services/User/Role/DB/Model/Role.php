<?php

namespace App\Services\User\Role\DB\Model;

use App\Services\User\User\DB\Model\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class Role
 * @package App\Services\User\Role\DB\Model
 */
final class Role extends Model implements RoleInterface
{
    /*__________________________________________________________________________________________________________________
    |
    |                                              CONSTANTS
    |___________________________________________________________________________________________________________________
    */

    /*
    |-------------------------------------------------------------------------------------------------------------------
    |                                                FIELDS
    |-------------------------------------------------------------------------------------------------------------------
    */

    const FIELD_NAME = 'name';

    /*
    |-------------------------------------------------------------------------------------------------------------------
    |                                                RELATIONS
    |-------------------------------------------------------------------------------------------------------------------
    */

    const RELATION_USERS = 'users';

    /*
    |-------------------------------------------------------------------------------------------------------------------
    |                                                VALUES
    |-------------------------------------------------------------------------------------------------------------------
    */

    const VALUE_SUPER_ADMIN = 'super admin';

    const VALUE_ADMIN = 'admin';

    const VALUE_EDITOR = 'editor';

    /*__________________________________________________________________________________________________________________
    |
    |                                              SETTINGS
    |___________________________________________________________________________________________________________________
    */

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_roles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_NAME,
    ];

    /*__________________________________________________________________________________________________________________
    |
    |                                              MAIN ATTRIBUTES
    |___________________________________________________________________________________________________________________
    */

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this|mixed
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    /*__________________________________________________________________________________________________________________
    |
    |                                              RELATIONS
    |___________________________________________________________________________________________________________________
    */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * @return Collection
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }
}

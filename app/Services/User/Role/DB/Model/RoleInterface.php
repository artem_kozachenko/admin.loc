<?php

namespace App\Services\User\Role\DB\Model;

use Illuminate\Support\Collection;

/**
 * Interface RoleInterface
 * @package App\Services\User\Role\DB\Model
 */
interface RoleInterface
{
    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param string $name
     * @return mixed
     */
    public function setName(string $name);

    /**
     * @return Collection
     */
    public function getUsers(): Collection;
}

<?php

namespace App\Services\User\Role\DB\Manager;

use App\Services\User\Role\DB\Model\Role;
use App\Utiles\EntityManager\EntityManager as BaseManager;

/**
 * Class Manager
 * @package App\Services\User\Role\DB\Manager
 */
final class Manager extends BaseManager implements ManagerInterface
{
    /**
     * @return string
     */
    protected function getClass()
    {
        return Role::class;
    }
}

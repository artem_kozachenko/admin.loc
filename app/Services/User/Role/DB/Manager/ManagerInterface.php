<?php

namespace App\Services\User\Role\DB\Manager;

use App\Utiles\EntityManager\EntityManagerInterface as BaseManagerInterface;

/**
 * Interface ManagerInterface
 * @package App\Services\User\Role\DB\Manager
 */
interface ManagerInterface extends BaseManagerInterface
{

}

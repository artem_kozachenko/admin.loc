<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('/register', 'Admin\Auth\RegisterController@register');
Route::post('/login', 'Admin\Auth\LoginController@login');
Route::post('/logout', 'Admin\Auth\LoginController@logout');

Route::post('/users', 'Admin\UserController@store');
Route::get('/users', 'Admin\UserController@index');
Route::put('/users/{id}', 'Admin\UserController@update');
Route::delete('/users/{id}', 'Admin\UserController@update');

<?php

return [
    'models' => [
        \App\Services\User\User\DB\Model\UserInterface::class =>
            \App\Services\User\User\DB\Model\User::class
    ],
    'managers' => [
        \App\Services\User\User\DB\Manager\ManagerInterface::class =>
            \App\Services\User\User\DB\Manager\Manager::class
    ],
    'repositories' => [
        \App\Services\User\User\DB\Repository\RepositoryInterface::class =>
            \App\Services\User\User\DB\Repository\Repository::class
    ],
    'helpers' => [
        \App\Utiles\ConfigUploader\ConfigUploaderInterface::class =>
            \App\Utiles\ConfigUploader\ConfigUploader::class,

        \App\Utiles\Resolve\ResolveHelperInterface::class =>
            \App\Utiles\Resolve\Resolve::class,

    ],
    'facades' => [
        \App\Utiles\Hash\HashInterface::class =>
            \App\Utiles\Hash\Hash::class
    ],
    'utiles' => [
        \App\Utiles\Repository\QueryObject\QueryObjectInterface::class =>
            \App\Utiles\Repository\QueryObject\QueryObject::class,

        \App\Utiles\Serializer\SerializerInterface::class =>
            \App\Utiles\Serializer\Serializer::class,

        \App\Utiles\Fractal\Response\FractalResponseInterface::class =>
            \App\Utiles\Fractal\Response\FractalResponse::class,

        \App\Utiles\Fractal\Serializers\JsonApiSerializer\JsonApiSerializerInterface::class =>
            \App\Utiles\Fractal\Serializers\JsonApiSerializer\JsonApiSerializer::class,

        \App\Utiles\Fractal\Fractal\FractalInterface::class =>
            \App\Utiles\Fractal\Fractal\Fractal::class,

        \App\Utiles\Fractal\Paginator\PaginatorInterface::class =>
            \App\Utiles\Fractal\Paginator\Paginator::class
    ]
];

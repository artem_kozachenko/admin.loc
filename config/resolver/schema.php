<?php

return [
    /*
     * 'schema' => ['object' => 'class']
     */
    'schema' => [
        /*______________________________________________________________________________________________________________
         |
         |                                          USER CONTEXT
         |______________________________________________________________________________________________________________
         */

        /*______________________________________________________________________________________________________________
         |
         |                                              AUTH
         |--------------------------------------------------------------------------------------------------------------
        */

        \App\Commands\User\Auth\RegisterCommand::class =>
            \App\Services\User\Auth\Handlers\RegisterHandler::class,

        \App\Commands\User\Auth\LogoutCommand::class =>
            \App\Services\User\Auth\Handlers\LogoutHandler::class,

        \App\Commands\User\Auth\LoginCommand::class =>
            \App\Services\User\Auth\Handlers\LoginHandler::class,

        /*______________________________________________________________________________________________________________
         |
         |                                              USER
         |--------------------------------------------------------------------------------------------------------------
        */

        \App\Commands\User\User\CreateUserCommand::class =>
            \App\Services\User\User\Handlers\CreateUserHandler::class,

        \App\Commands\User\User\UpdateUserCommand::class =>
            \App\Services\User\User\Handlers\UpdateUserHandler::class,

        \App\Commands\User\User\DeleteUserCommand::class =>
            \App\Services\User\User\Handlers\DeleteUserHandler::class,

        \App\Commands\User\User\GetUsersCommand::class =>
            \App\Services\User\User\Handlers\GetUsersHandler::class,
    ],
];

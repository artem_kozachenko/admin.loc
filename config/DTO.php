<?php

return [
    /*
     |__________________________________________________________________________________________________________________
     |
     |                          OBJECTS SECTION
     |__________________________________________________________________________________________________________________
     |
     | Here you may specify the objects and builders for them by some alias.
     | NB! If you want declare composite object, you should specify composite of which object it is
     |
     | 'objects' => [
     |      DTO with assembler
     |      'alias' => [
     |          'builder' => object alias from this section
     |          'object' => Composite object class name
     |      ]
     |      DTO without assembler
     |      'alias' => [
     |          'object' => Composite object class name
     |      ]
     |]
     */
    'objects' => [

    ],

    /*
     |__________________________________________________________________________________________________________________
     |
     |                          CONTEXTS SECTION
     |__________________________________________________________________________________________________________________
     |
     | Here you may specify the objects and builders for them by some alias.
     | NB! If you want declare composite object, you should specify composite of which object it is
     |
     | 'contexts' => [
     |      'alias' => [
     |          'type' => single/multiple
     |          'DTOsAliases' => alias - string/array
     |      ]
     |]
     */
    'contexts' => [

    ],
];

<?php

return [
/*    'parser' => [
        'validator' => 'compositeValidator',
        'validatorsAliases' => ['required', 'unsignedInt', 'minValue', 'biggerThen'],
        'configs' => [
            'default' => [
                'required' => [
                    'ownerId',
                    'vehiclePrice',
                    'term',
                    'coverLimit',
                    'gapTypeName',
                    'cost',
                ],
                'unsignedInt' => [
                    'ownerId',
                    'term',
                    'coverLimit',
                    'vehiclePrice',
                    'cost',
                ],
                'minValue' => [
                    ['field' => 'vehiclePrice', 'value' => 5000]
                ],
                'biggerThen' => [
                    ['bigger' => 'vehiclePrice', 'smaller' => 'coverLimit']
                ],
            ],
            'direct_gap' => [
                'required' => [
                    'ownerId',
                    'vehiclePrice',
                    'term',
                    'gapTypeName',
                    'cost',
                ],
                'unsignedInt' => [
                    'ownerId',
                    'term',
                    'vehiclePrice',
                    'cost',
                ],
                'minValue' => [
                    [
                        'field' => 'vehiclePrice',
                        'value' => 5000
                    ]
                ],
                'biggerThen' => [
                    [
                        'bigger' => 'vehiclePrice',
                        'smaller' => 'coverLimit'
                    ]
                ],
            ],
        ],
    ],*/
    'fractal' => [
        'validator' => 'type',
        'configs' => [
            'default' => [
                'type' => ['isArray', 'isCollection', 'isModel', 'isLengthAwarePaginator']
            ],
        ]
    ],
];

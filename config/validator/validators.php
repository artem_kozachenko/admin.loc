<?php

return [
    'compositeValidator' =>
        \App\Utiles\Validator\ValidatorComposite::class,
    'required' =>
        \App\Utiles\Validator\Validators\Required::class,
    'unsignedInt' =>
        \App\Utiles\Validator\Validators\UnsignedInt::class,
    'minValue' =>
        \App\Utiles\Validator\Validators\MinValue::class,
    'biggerThen' =>
        \App\Utiles\Validator\Validators\BiggerThen::class,
    'type' =>
        \App\Utiles\Validator\Validators\Type::class,
];

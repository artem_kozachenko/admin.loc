<?php

return [
    'objects' => [

        /*--------------------------------------------------------------------------------------------------------------
         *                                                      QUERY OBJECTS
         *--------------------------------------------------------------------------------------------------------------
         */

        \App\Services\User\User\DB\QueryObject\QueryObject::class => [
            'map' => [
                'id' => 'setId',
                'name' => 'setName',
                'email' => 'setEmail'
            ],
            'group' => 'queryObjects'
        ],

        /*--------------------------------------------------------------------------------------------------------------
         *                                                          DTOs
         *--------------------------------------------------------------------------------------------------------------
         */

    ],
    'groups' => [
        'queryObjects' => [
            'select' => 'setSelect',
            'with' => 'setWith',
            'where' => 'setWhere',
            'whereBetween' => 'setWhereBetween',
            'whereNotBetween' => 'setWhereNotBetween',
            'whereIn' => 'setWhereIn',
            'whereNotIn' => 'setWhereNotIn',
            'whereNull' => 'setWhereNull',
            'whereNotNull' => 'setWhereNotNull',
            'whereDate' => 'setWhereDate',
            'whereYear' => 'setWhereYear',
            'whereMonth' => 'setWhereMonth',
            'whereDay' => 'setWhereDay',
            'whereTime' => 'setWhereTime',
            'whereColumn' => 'setWhereColumn',
            'whereHas' => 'setWhereHas',
            'has' => 'setHas',
            'distinct' => 'setDistinct',
            'orderBy' => 'setOrderBy',
            'groupBy' => 'setGroupBy',
            'having' => 'setHaving',
            'limit' => 'setLimit',
            'offset' => 'setOffset',
            'perPage' => 'setPerPage'
        ]
    ]
];

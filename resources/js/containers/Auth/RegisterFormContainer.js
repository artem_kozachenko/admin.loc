import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import RegisterForm from '../../components/Auth/RegisterForm/RegisterForm';
import { thunkedRegister } from '../../store/Auth/actions';

const Proxy = (props) => {
    return (
        <RegisterForm onSubmit={
            (values) => {
                return props.handleSubmit(
                    values.name,
                    values.email,
                    values.password,
                    values.password_confirmation,
                    props.history,
                );
            }
        } />
    )
};
const mapDispatchToProps = (dispatch) => {
    return {
        handleSubmit:
            (
                name,
                email,
                password,
                passwordConfirmation,
                history
            ) => {
                return dispatch(thunkedRegister(
                    name,
                    email,
                    password,
                    passwordConfirmation,
                    history
                ));
            }
    };
};

export default connect(null, mapDispatchToProps)(withRouter(Proxy));

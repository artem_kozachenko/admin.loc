import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import LoginForm from '../../components/Auth/LoginForm/LoginForm';
import { thunkedLogin } from '../../store/Auth/actions';

const Proxy = (props) => {
    return (
        <LoginForm onSubmit={
            (values) => {
                return props.handleSubmit(
                    values.email,
                    values.password,
                    props.history
                );
            }
        } />
    )
};
const mapDispatchToProps = (dispatch) => {
    return {
        handleSubmit:
            (email, password, history) => {
                return dispatch(thunkedLogin(email, password, history));
            }
    };
};

export default connect(null, mapDispatchToProps)(withRouter(Proxy));

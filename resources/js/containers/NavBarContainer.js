import React from 'react';
import { connect } from 'react-redux';

import NavBar from '../components/NavBar/NavBar';

const mapStateToProps = (state) => {
    return {
        auth: state.auth,
        navBarVisibility: state.navBarVisibility.open
    };
};

export default connect(mapStateToProps)(NavBar);

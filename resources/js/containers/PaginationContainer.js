import React from 'react';
import { connect } from 'react-redux';

import PaginationContainer from '../components/Pagination/Pagination';
import { thunkedSetCurrentPage, handlePageChange } from '../store/Pagination/actions';

const mapStateToProps = (state) => {
    return { pagination: state.pagination };
};
const mapDispatchToProps = (dispatch) => {
    return {
        thunkedSetCurrentPage: () => {
            dispatch(thunkedSetCurrentPage());
        },
        handlePageChange: (data) => {
            dispatch(handlePageChange(data));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(PaginationContainer);

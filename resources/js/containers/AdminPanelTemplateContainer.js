import React from 'react';
import { connect } from 'react-redux';
import AdminPanelTemplate from '../components/Dashboard/AdminPanelTemplate';

const mapStateToProps = (state) => {
    return { navBarVisibility: state.navBarVisibility.open };
};

export default connect(mapStateToProps)(AdminPanelTemplate);

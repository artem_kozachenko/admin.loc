import React from 'react';
import { connect } from 'react-redux';

import ProtectedRenderedComponent from '../../components/Routes/Protected/RenderedComponent/ProtectedRenderedComponent';

const mapStateToProps = (state) => {
    return { auth: state.auth }
};

export default connect(mapStateToProps)(ProtectedRenderedComponent);

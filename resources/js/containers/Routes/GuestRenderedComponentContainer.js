import React from 'react';
import { connect } from 'react-redux';

import GuestRenderedComponent from '../../components/Routes/Guest/RenderedComponent/GuestRenderedComponent';

const mapStateToProps = (state) => {
    return { auth: state.auth }
};

export default connect(mapStateToProps)(GuestRenderedComponent);

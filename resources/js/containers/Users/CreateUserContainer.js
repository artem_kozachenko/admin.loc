import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import CreateUser from '../../components/Users/CreateUser/CreateUser';
import { thunkedCreateUser } from '../../store/Users/actions';

const Proxy = (props) => {
    return (
        <CreateUser onSubmit={
            (values) => {
                return props.handleSubmit(
                    values.name,
                    values.email,
                    values.password,
                    values.password_confirmation,
                    props.history,
                );
            }
        }/>
    );
};
const mapDispatchToProps = (dispatch) => {
    return {
        handleSubmit:
            (
                name,
                email,
                password,
                passwordConfirmation,
                history,
            ) => {
                return dispatch(thunkedCreateUser(
                    name,
                    email,
                    password,
                    passwordConfirmation,
                    history,
                ));
            }
    };
};

export default connect(null, mapDispatchToProps)(withRouter(Proxy));

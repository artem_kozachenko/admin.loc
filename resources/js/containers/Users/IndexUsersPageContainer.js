import React from 'react';
import { connect } from 'react-redux';

import { thunkedConfigurePageState, thunkedHandlePageChange, popStateListener } from '../../store/Users/actions';
import IndexUsersPage from '../../components/Users/IndexUsers/IndexUsersPage';

const mapStateToProps = (state) => {
    return { indexUsers: state.indexUsers };
};
const mapDispatchToProps = (dispatch) => {
    return {
        popStateListener: () => { return dispatch(popStateListener()); },
        configurePageState: () => { return dispatch(thunkedConfigurePageState()); },
        handlePageChange: (data) => { return dispatch(thunkedHandlePageChange(data)); }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(IndexUsersPage);

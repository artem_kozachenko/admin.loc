import React from 'react';
import { connect } from 'react-redux';

import Header from '../components/Header/Header';
import { thunkedLogout } from '../store/Auth/actions';
import { toggleNavBar } from '../store/Header/actions';

const mapStateToProps = (state) => {
    return {
        auth: state.auth,
        navBarVisibility: state.navBarVisibility.open
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
        handleClick: (history) => {
            dispatch(thunkedLogout(history));
        },
        handleNavBarToggle: (open) => {
            dispatch(toggleNavBar(open));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);

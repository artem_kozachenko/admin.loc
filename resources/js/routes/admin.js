const API = '/api/';
export const routes = {
    AUTH: {
        LOGIN: `${API}login`,
        REGISTER: `${API}register`,
        LOGOUT: `${API}logout`,
    },
    USERS: {
        INDEX: `${API}users`,
    },
};

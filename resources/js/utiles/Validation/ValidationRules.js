export const required = (value) => {
    return ! value
        ? 'Required'
        : undefined;
};

export const maxLength = (max) => (value) => {
    return value && value.length > max
        ? `Must be ${max} characters or less`
        : undefined;
};

export const minLength = (min) => (value) => {
    return value && value.length < min
        ? `Must be ${min} characters or more`
        : undefined;
};

export const number = (value) => {
    return value && isNaN(Number(value))
        ? 'Must be a number'
        : undefined;
};

export const minValue = (min) => (value) => {
    return value && value < min
        ? `Must be at least ${min}`
        : undefined;
};

export const maxValue = (max) => (value) => {
    return value && value > max
        ? `Must be less then ${max}`
        : undefined;
};

export const email = (value) => {
    return value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
        ? 'Invalid email address'
        : undefined;
};

export const passwordsMatch = (value, allValues) => {
    return value !== allValues.password
        ? 'Passwords don\'t match'
        : undefined;
};

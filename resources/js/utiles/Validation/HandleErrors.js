import { SubmissionError } from 'redux-form';

const handleErrors = (error) => {
    let errors = error.response.data.errors;
    let response = {};
    for (let error in errors) {
        response[error] = errors[error][0];
    }
    throw new SubmissionError(response);
};

export default handleErrors;
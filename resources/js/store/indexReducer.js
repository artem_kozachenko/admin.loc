import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import { auth } from './Auth/reducers';
import { navBarVisibility } from './Header/reducers';
import indexUsers from './Users/reducer'
import { pagination } from './Pagination/reducers';

export const indexReducer = combineReducers({
    form: formReducer,
    auth,
    navBarVisibility,
    indexUsers,
    pagination
});
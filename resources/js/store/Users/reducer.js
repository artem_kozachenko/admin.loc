import undoable, { includeAction } from 'redux-undo';

import C from './constants';

let indexUsers = (state = {}, action) => {
    switch (action.type) {
        case C.CONFIGURE_PAGE_STATE:
            return {
                currentPage: action.payload.currentPage,
                pageCount: action.payload.pageCount,
                users: action.payload.users
            };
        default :
            return state;
    }
};
indexUsers = undoable(
    indexUsers,
    {
        filter: includeAction([C.CONFIGURE_PAGE_STATE]),
        ignoreInitialState: true
    }
);

export default indexUsers;

import React from 'react';
import axios from 'axios';
import { ActionCreators as UndoActionCreators } from 'redux-undo';

import C from './constants';
import handleErrors from '../../utiles/Validation/HandleErrors';
import { routes as AdminRoutes } from '../../routes/admin';
import { getQueryStringValue } from '../../utiles/Url/Url';

export const configurePageState = (currentPage, pageCount, users) => {
    return {
        type: C.CONFIGURE_PAGE_STATE,
        payload: {
            currentPage,
            pageCount,
            users: [...users]
        }
    };
};

export const thunkedConfigurePageState = () => {
    return (dispatch, getState) => {
        let currentPage = getQueryStringValue('page');
        currentPage = currentPage
            ? currentPage
            : 1;

        return configureStore(dispatch, getState, currentPage);
    };
};

export const thunkedHandlePageChange = (data) => {
    return (dispatch, getState) => {
        const selectedPage = data.selected;
        const currentPage = selectedPage >= 0 && selectedPage < getState().indexUsers.present.pageCount
            ? selectedPage + 1
            : 1;
        const newUrl =
            window.location.protocol +
            '//' +
            window.location.host +
            window.location.pathname +
            '?page=' +
            currentPage;
        window.history.pushState({ path: newUrl }, '', newUrl);

        return configureStore(dispatch, getState, currentPage);
    };
};

export const thunkedCreateUser = (
    name,
    email,
    password,
    passwordConfirmation,
    history,
) => {
    return (dispatch, getState) => {
        return axios({
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + getState().auth.token
            },
            method: 'post',
            url: AdminRoutes.USERS.INDEX,
            data: {
                name,
                email,
                password,
                password_confirmation: passwordConfirmation,
            }
        })
            .then((response) => {
                history.push('/users');
            })
            .catch((error) => {
                handleErrors(error);
            });
    };
};

export const popStateListener = () => {
    return (dispatch, getState) => {
        const indexUsers = getState().indexUsers;
        let currentPage = getQueryStringValue('page');
        currentPage = currentPage
            ? currentPage
            : 1;
        const past = indexUsers.past;
        const future = indexUsers.future;
        if (past.length > 1) {
            const pastCurrentPage = past[past.length - 1].currentPage;
            if (currentPage == pastCurrentPage) {
                return dispatch(UndoActionCreators.undo());
            }
        }
        if (future.length > 0) {
            const futureCurrentPage = future[0].currentPage;
            if (currentPage == futureCurrentPage) {
                return dispatch(UndoActionCreators.redo());
            }
        }

        return dispatch(thunkedConfigurePageState());
    };
};

const configureStore = (dispatch, getState, currentPage) => {
    return axios({
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + getState().auth.token
        },
        method: 'get',
        url: AdminRoutes.USERS.INDEX + '?page=' + currentPage,
    })
        .then((response) => {
            return response.data;
        })
        .then((response) => {
            dispatch(configurePageState(currentPage, response.meta.last_page, response.data));
        })
        .catch((error) => {
            console.error(error);
        });
};

const ID = '@@USERS_';
const constants = {
    CONFIGURE_PAGE_STATE: `${ID}CONFIGURE_PAGE_STATE`,
};

export default constants;

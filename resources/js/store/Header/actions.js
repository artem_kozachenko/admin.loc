import React from 'react';

import C from './constants';

export const toggleNavBar = (open) => {
    return {
        type: C.TOGGLE_NAV_BAR,
        payload: { open }
    };
};

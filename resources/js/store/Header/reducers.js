import C from './constants';

const initialState = { open: false };

export const navBarVisibility = (state = initialState, action) => {
    switch (action.type) {
        case C.TOGGLE_NAV_BAR:
            return {
                open: action.payload.open,
            };
        default :
            return state;
    }
};

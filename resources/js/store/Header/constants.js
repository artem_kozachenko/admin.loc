const ID = '@@HEADER_';
const constants = { TOGGLE_NAV_BAR: `${ID}TOGGLE_NAV_BAR` };

export default constants;
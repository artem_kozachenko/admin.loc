import React from 'react';

import C from './constants';
import { getQueryStringValue } from '../../utiles/Url/Url';

export const setCurrentPage = (currentPage) => {
    return {
        type: C.SET_CURRENT_PAGE,
        payload: { currentPage }
    };
};

export const setPageCount = (pageCount) => {
    return {
        type: C.SET_PAGE_COUNT,
        payload: { pageCount }
    };
};

export const setPagination = (pageCount, currentPage) => {
    return {
        type: C.SET_PAGINATION,
        payload: {
            pageCount,
            currentPage
        }
    };
};

export const handlePageChange = (data) => {
    return (dispatch) => {
        const page = data.selected >= 0
            ? data.selected + 1
            : 1;
        dispatch(setCurrentPage(page));
        const newUrl =
            window.location.protocol +
            '//' +
            window.location.host +
            window.location.pathname +
            '?page=' +
            page;
        window.history.pushState({ path: newUrl }, '', newUrl);
    };
};

export const thunkedSetCurrentPage = () => {
    return (dispatch, getState) => {
        let page = getQueryStringValue('page');
        const pagination = getState().pagination;
        page = page && page <= pagination.pageCount
            ? page
            : 1;
        dispatch(setCurrentPage(page));
    };
};

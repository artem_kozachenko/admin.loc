import C from './constants';

const initialState = { pageCount: 1, currentPage: 1 };

export const pagination = (state = initialState, action) => {
    switch (action.type) {
        case C.SET_CURRENT_PAGE:
            return {
                pageCount: state.pageCount,
                currentPage: action.payload.currentPage
            };
        case C.SET_PAGE_COUNT:
            return {
                pageCount: state.pageCount,
                currentPage: action.payload.currentPage
            };
        case C.SET_PAGINATION:
            return {
                pageCount: action.payload.pageCount,
                currentPage: action.payload.currentPage
            };
        default :
            return state;
    }
};

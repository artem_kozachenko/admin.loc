const ID = '@@PAGINATION_';
const constants = {
    SET_CURRENT_PAGE: `${ID}SET_CURRENT_PAGE`,
    SET_PAGE_COUNT: `${ID}SET_PAGE_COUNT`,
    SET_PAGINATION: `${ID}SET_PAGINATION`
};

export default constants;
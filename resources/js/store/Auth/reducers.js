import C from './constants';

const initialState = { email: '', token: '' };

export const auth = (state = initialState, action) => {
    switch (action.type) {
        case C.AUTH:
            return {
                email: action.payload.email,
                token: action.payload.token
            };
        default :
            return state;
    }
};

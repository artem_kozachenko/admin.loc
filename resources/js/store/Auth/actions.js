import React from 'react';
import axios from 'axios';

import C from './constants';
import handleErrors from '../../utiles/Validation/HandleErrors';
import { routes as AdminRoutes } from '../../routes/admin';

export const auth = (email, token) => {
    return {
        type: C.AUTH,
        payload: { email, token }
    };
};

export const thunkedRegister = (
    name,
    email,
    password,
    passwordConfirmation,
    history
) => {
    return (dispatch) => {
        return axios({
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            url: AdminRoutes.AUTH.REGISTER,
            data: {
                name,
                email,
                password,
                password_confirmation: passwordConfirmation
            }
        })
            .then((response) => {
                dispatch(auth(email, response.data.accessToken));
                history.push('/');
            })
            .catch((error) => {
                handleErrors(error);
            });
    };
};

export const thunkedLogin = (email, password, history) => {
    return (dispatch) => {
        return axios({
            headers: { 'Content-Type': 'application/json' },
            method: 'post',
            url: AdminRoutes.AUTH.LOGIN,
            data: { email, password }
        })
            .then((response) => {
                dispatch(auth(email, response.data.accessToken));
                history.push('/');
            })
            .catch((error) => {
                handleErrors(error);
            });
    };
};

export const thunkedLogout = (history) => {
    return (dispatch, getState) => {
        const { email, token } = getState().auth;

        return axios({
            method: 'post',
            url: AdminRoutes.AUTH.LOGOUT,
            data: { email },
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            },
        })
            .then((response) => {
                dispatch(auth('', ''));
                history.push('/login');
            })
            .catch((error) => {
                console.log(error.response);
            });
    };
};

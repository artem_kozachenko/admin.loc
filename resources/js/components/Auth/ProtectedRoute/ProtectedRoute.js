import React from 'react';
import { Redirect, Route } from 'react-router-dom';

const ProtectedRoute = ({ Component, ...rest }) => {
    return (
        <Route { ...rest } render={(props) => {
            return !!rest.auth.token
                ? (<Component { ...rest } />)
                : (<Redirect to={ { pathname: '/login' } } />);
        }} />
    );
};

export default ProtectedRoute;

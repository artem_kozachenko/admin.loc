import React from 'react';
import { Field, reduxForm } from 'redux-form';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

import { required, email, minLength } from '../../../utiles/Validation/ValidationRules';
import Input from '../../Base/Input/Input';

const passwordMinLength = minLength(6);
const LoginForm = (props) => {
    return (
        <Grid container
              style={ { height: '100%' } }
              direction="row"
              justify="center"
              alignItems="center"
        >
            <Grid item lg={ 5 } >
                <Paper elevation={ 1 }>
                    <Grid container justify="center" alignItems="center">
                        <Grid item lg={ 10 }>
                            <form onSubmit={ props.handleSubmit } style={ { textAlign: 'center' } }>
                                <div style={ { margin: 20 } }>
                                    <Field name="email"
                                           component={ Input }
                                           label="Email"
                                           validate={ [required, email] }
                                    />
                                </div>
                                <div style={ { margin: 20 } }>
                                    <Field name="password"
                                           component={ Input }
                                           label="Password"
                                           validate={ [required, passwordMinLength] }
                                           type="password"
                                    />
                                </div>
                                <div style={ { margin: 20 } }>
                                    <Button type="submit" variant="contained" color="primary">Log in</Button>
                                </div>
                            </form>
                        </Grid>
                    </Grid>
                </Paper>
            </Grid>
        </Grid>
    );
};

export default reduxForm({ form: 'login' })(LoginForm);

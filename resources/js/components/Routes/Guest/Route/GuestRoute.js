import React from 'react';
import { Route } from 'react-router-dom';

import GuestRenderedComponentContainer from '../../../../containers/Routes/GuestRenderedComponentContainer';

const GuestRoute = ({ Component, ...rest }) => {
    return (
        <Route { ...rest } render={
            (props) => {
                return <GuestRenderedComponentContainer Component={ Component } { ...rest } />
            }
        }/>
    );
};

export default GuestRoute;

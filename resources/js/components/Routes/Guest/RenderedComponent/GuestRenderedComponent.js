import React from 'react';
import { Redirect } from 'react-router-dom';

const GuestRenderedComponent = ({ Component, ...rest }) => {
    return !!rest.auth.token
        ? (<Redirect to={ { pathname: '/' } } />)
        : (<Component { ...rest } />);
};

export default GuestRenderedComponent;
import React from 'react';
import { Redirect } from 'react-router-dom';

const ProtectedRenderedComponent = ({ Component, ...rest }) => {
    return !!rest.auth.token
        ? (<Component { ...rest } />)
        : (<Redirect to={ { pathname: '/login' } } />);
};

export default ProtectedRenderedComponent;
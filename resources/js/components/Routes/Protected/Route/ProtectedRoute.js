import React from 'react';
import { Route } from 'react-router-dom';

import ProtectedRenderedComponentContainer from '../../../../containers/Routes/ProtectedRenderedComponentContainer';

const ProtectedRoute = ({ Component, ...rest }) => {
    return (
        <Route { ...rest } render={
            (props) => {
                return <ProtectedRenderedComponentContainer Component={ Component } { ...rest } />
            }
        }/>
    );
};

export default ProtectedRoute;

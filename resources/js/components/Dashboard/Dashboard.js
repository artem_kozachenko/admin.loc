import React from 'react';
import { withStyles } from '@material-ui/core/styles';

const styles = (theme) => {
    return {
        toolbar: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'flex-end',
            padding: '0 8px',
        },
        content: {
            flexGrow: 1,
            padding: theme.spacing.unit * 3,
        }
    };
};

const Dashboard = (props) => {
    const { classes, theme } = props;

    return (
        <main className={classes.content}>
            <div className={classes.toolbar} />
            <h1>DASHBOARD</h1>
        </main>
    )
};

export default withStyles(styles, { withTheme: true })(Dashboard);

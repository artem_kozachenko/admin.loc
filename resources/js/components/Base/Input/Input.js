import React from 'react';
import TextField from '@material-ui/core/es/TextField/TextField';

const Input = ({
    label,
    input,
    meta: { touched, invalid, error },
    ...custom
}) => {
    return (
        <TextField
            style={ { width: '100%' } }
            label={ label }
            placeholder={ label }
            error={ touched && invalid }
            helperText={ touched && error }
            { ...input }
            { ...custom }
        />
    );
};

export default Input;

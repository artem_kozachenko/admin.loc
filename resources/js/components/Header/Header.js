import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';

import styles from './styles';

function Header(props) {
    const { classes } = props;

    return (
        <AppBar position="fixed">
            <Toolbar disableGutters={ true } classes={ { root: classes.headerElements } }>
                <div style={ { display: 'flex', marginLeft: '24px' } }>
                    {
                        !!props.auth.token
                            ? (
                                <IconButton
                                    color="inherit"
                                    aria-label="Open drawer"
                                    onClick={
                                        () => {
                                            props.handleNavBarToggle(! props.navBarVisibility);
                                        }
                                    }
                                    className={ classes.menuButton }
                                >
                                    <MenuIcon />
                                </IconButton>
                            )
                            : ('')
                    }
                    <Typography className={ classes.btnLink }
                                variant="h6"
                                color="inherit"
                                noWrap
                                to="/"
                                component={ Link }
                    >
                        AdminPanel
                    </Typography>
                </div>
                <div style={ { marginRight: '24px' } }>
                    {
                        !!props.auth.token
                            ? (
                                <Button onClick={
                                    () => {
                                        props.handleClick(props.history)
                                    }
                                } color="inherit" className={ classes.btnLink }>
                                    Logout
                                </Button>
                            )
                            : (
                                <>
                                    <Button color="inherit"
                                            className={ classes.btnLink }
                                            to="/login"
                                            component={ Link }
                                    >
                                        Login
                                    </Button>
                                    <Button color="inherit"
                                            className={ classes.btnLink }
                                            to="/register"
                                            component={ Link }
                                    >
                                        Register
                                    </Button>
                                </>
                            )
                    }
                </div>
            </Toolbar>
        </AppBar>
    );
}

export default withRouter(withStyles(styles)(Header));
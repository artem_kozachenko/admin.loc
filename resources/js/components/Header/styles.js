const styles = () => {
    return {
        menuButton: {
            margin: '0 24px 0 0',
            padding: 0
        },
        btnLink: {
            '&:hover': {
                color: '#fff',
                textDecoration: 'none'
            }
        },
        headerElements: {
            justifyContent: 'space-between'
        }
    }
};

export default styles;
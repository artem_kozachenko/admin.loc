import React from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Dashboard from '@material-ui/icons/Dashboard';
import SupervisedUserCircle from '@material-ui/icons/SupervisedUserCircle';
import { Link } from 'react-router-dom';

const drawerWidth = 240;
const styles = theme => ({
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
    },
    drawerOpen: {
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerClose: {
        transition: theme.transitions.create(
            'width',
            {
                easing: theme.transitions.easing.sharp,
                duration: theme.transitions.duration.leavingScreen,
            }
        ),
        overflowX: 'hidden',
        width: theme.spacing.unit * 7 + 1,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing.unit * 9 + 1,
        },
    },
    top: {
        top: '64px',
        height: 'calc(100vh - 64px)',
    },
    navIcon: {
        width: '1.6em'
    },
});
const NavBar = (props) => {
    const { classes, theme } = props;

    return (
        <>
            {
                !!props.auth.token
                    ? (
                        <Drawer
                            variant="permanent"
                            className={ classNames(
                                classes.drawer,
                                {
                                    [classes.drawerOpen]: props.navBarVisibility,
                                    [classes.drawerClose]: !props.navBarVisibility,
                                }
                            )}
                            classes={{ paper: classNames(
                                    classes.top,
                                    {
                                        [classes.drawerOpen]: props.navBarVisibility,
                                        [classes.drawerClose]: !props.navBarVisibility,
                                    }
                                )}}
                            open={ props.navBarVisibility }
                        >
                            <Divider />
                            <List>
                                {/*{['Dashboard', 'Users'].map((text, index) => (*/}
                                    {/*<ListItem key={text} to={index % 2 === 0 ? '/dashboard' : '/users' } component={ Link }>*/}
                                        {/*<ListItemIcon>*/}
                                            {/*{*/}
                                                {/*index % 2 === 0*/}
                                                    {/*? <Dashboard classes={ { root: classes.qwe } } />*/}
                                                    {/*: <SupervisedUserCircle classes={ { root: classes.qwe } } />*/}
                                            {/*}*/}
                                        {/*</ListItemIcon>*/}
                                        {/*<ListItemText primary={text} />*/}
                                    {/*</ListItem>*/}
                                {/*))}*/}
                                <ListItem button to="/" component={ Link }>
                                    <ListItemIcon>
                                        <Dashboard classes={ { root: classes.navIcon } } />
                                    </ListItemIcon>
                                    <ListItemText primary="Dashboard" />
                                </ListItem>
                                <ListItem button to="/users" component={ Link }>
                                    <ListItemIcon>
                                        <SupervisedUserCircle classes={ { root: classes.navIcon } } />
                                    </ListItemIcon>
                                    <ListItemText primary="Users" />
                                </ListItem>
                            </List>
                        </Drawer>
                    )
                    : ('')
            }
        </>
    )
};

export default withStyles(styles, { withTheme: true })(NavBar);

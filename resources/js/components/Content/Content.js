import React from 'react';

import LoginFormContainer from '../../containers/Auth/LoginFormContainer';
import RegisterFormContainer from '../../containers/Auth/RegisterFormContainer';
import ProtectedRoute from '../Routes/Protected/Route/ProtectedRoute';
import GuestRoute from '../Routes/Guest/Route/GuestRoute';
import Dashboard from '../Dashboard/Dashboard';
import CreateUserContainer from '../../containers/Users/CreateUserContainer';
import IndexUsersPageContainer from '../../containers/Users/IndexUsersPageContainer'

const Content = (props) => {
    return (
        <div className="container" style={ { height: 'calc(100vh - 64px)', position: 'relative', top: '64px' } }>
            <ProtectedRoute exact path="/" Component={ Dashboard } />
            <ProtectedRoute exact path="/users" Component={ IndexUsersPageContainer } />
            <ProtectedRoute path="/users/new" Component={ CreateUserContainer } />
            <GuestRoute path="/login" Component={ LoginFormContainer } />
            <GuestRoute path="/register" Component={ RegisterFormContainer } />
        </div>
    );
};

export default Content;

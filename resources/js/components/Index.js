import React from 'react';
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { PersistGate } from 'redux-persist/integration/react';

import App from './App/App';
import StoreConfig from '../config/store';

const render = () => {
    ReactDOM.render(
        <Provider store={ StoreConfig.store }>
            <PersistGate loading={ null } persistor={ StoreConfig.persistor }>
                <BrowserRouter>
                    <App />
                </BrowserRouter>
            </PersistGate>
        </Provider>,
        document.getElementById('index')
    );
};
StoreConfig.store.subscribe(render);
render();

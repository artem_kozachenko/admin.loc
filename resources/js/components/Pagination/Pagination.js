import React from 'react';
import ReactPaginate from 'react-paginate';

const Pagination = ({ currentPage, pageCount, handlePageChange }) => {
    return (
        <ReactPaginate
            pageCount={ pageCount }
            initialPage={ currentPage - 1 }
            forcePage={ currentPage - 1 }
            pageRangeDisplayed={ 4 }
            marginPagesDisplayed={ 2 }
            previousLabel="&#x276E;"
            nextLabel="&#x276F;"
            containerClassName="uk-pagination uk-flex-center"
            activeClassName="uk-active"
            disabledClassName="uk-disabled"
            onPageChange={ handlePageChange }
            disableInitialCallback={ true }
        />
    );
};

export default Pagination;

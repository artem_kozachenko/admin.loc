import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';

import Pagination from '../../Pagination/Pagination';
import UsersTable from './UsersTable/UsersTable';

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
    },
});

class IndexUsersPage extends React.Component {
    constructor(props) {
        super(props);

        window.addEventListener('popstate', props.popStateListener);
    }

    componentDidMount() {
        this.props.configurePageState();
    }
    componentWillUnmount() {
        alert('qwe')
    }

    render() {
        const { currentPage, pageCount, users } = this.props.indexUsers.present;
        return (
            <div style={ { marginTop: '20px' } }>
                <Button variant="contained" component={ Link } to="/users/new">
                    Create
                </Button>
                <Paper className={ this.props.classes.root }>
                    <UsersTable users={ users }  />
                </Paper>
                <Pagination currentPage={ currentPage }
                            pageCount={ pageCount }
                            handlePageChange={ this.props.handlePageChange }
                />
            </div>
        );
    }
}

export default withStyles(styles)(IndexUsersPage);

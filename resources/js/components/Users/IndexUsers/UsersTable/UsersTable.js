import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
    },
    table: {
        minWidth: 500,
    },
    tableWrapper: {
        overflowX: 'auto',
    },
});
const UsersTable = ({ users, classes }) => {
    return (
        <div className={ classes.tableWrapper }>
            <Table className={ classes.table }>
                <TableHead>
                    <TableRow>
                        <TableCell align="center">Name</TableCell>
                        <TableCell align="center">Email</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {
                        users.map(
                            user => {
                                return (
                                    <TableRow key={ user.id }>
                                        <TableCell align="center">{ user.name }</TableCell>
                                        <TableCell align="center">{ user.email }</TableCell>
                                    </TableRow>
                                );
                            }
                        )
                    }
                </TableBody>
            </Table>
        </div>
    );
};

export default withStyles(styles)(UsersTable);




















// import React from 'react';
// import { withStyles } from '@material-ui/core/styles';
// import Table from '@material-ui/core/Table';
// import TableBody from '@material-ui/core/TableBody';
// import TableHead from '@material-ui/core/TableHead';
// import TableCell from '@material-ui/core/TableCell';
// import TableRow from '@material-ui/core/TableRow';
// import Paper from '@material-ui/core/Paper';
// import Button from '@material-ui/core/Button';
// import { Link } from 'react-router-dom';
//
// const styles = theme => ({
//     root: {
//         width: '100%',
//         marginTop: theme.spacing.unit * 3,
//     },
//     table: {
//         minWidth: 500,
//     },
//     tableWrapper: {
//         overflowX: 'auto',
//     },
// });
//
// class UserTable extends React.Component {
//     constructor(props) {
//         super(props);
//
//         this.props.getUsers(this.props.currentPage);
//     }
//
//     // componentDidMount() {
//     //     this.props.getUsers(this.props.currentPage);
//     // }
//
//     componentDidUpdate(prevProps, prevState, snapshot) {
//         // console.log('UserTable - componentDidUpdate')
//         //
//         // console.log('prevProps.currentPage', prevProps.currentPage);
//         // console.log('this.props.currentPage', this.props.currentPage);
//         if (this.props.currentPage != prevProps.currentPage) {
//             this.props.getUsers(this.props.currentPage);
//         }
//     }
//
//     render() {
//         const { classes } = this.props;
//         const rowsPerPage = 10;
//         const emptyRows = rowsPerPage - Math.min(rowsPerPage, this.props.users.length - this.props.currentPage * this.props.rowsPerPage);
//
//         return (
//             <Paper className={classes.root}>
//                 <div className={classes.tableWrapper}>
//                     <Button variant="contained" className={classes.button} component={ Link } to="/users/new">
//                         Create
//                     </Button>
//                     <Table className={classes.table}>
//                         <TableHead>
//                             <TableRow>
//                                 <TableCell align="center">Name</TableCell>
//                                 <TableCell align="center">Email</TableCell>
//                                 <TableCell align="center">Manage</TableCell>
//                             </TableRow>
//                         </TableHead>
//                         <TableBody>
//                             {this.props.users.slice(0, rowsPerPage).map(row => {
//                                 return (
//                                     <TableRow key={row.id}>
//                                         <TableCell component="th" scope="row">
//                                             {row.name}
//                                         </TableCell>
//                                         <TableCell align="right">{row.email}</TableCell>
//                                         <TableCell align="right">
//                                             <Button variant="contained" className={classes.button}>
//                                                 Show
//                                             </Button>
//                                             <Button variant="contained" color="primary" className={classes.button} component={ Link } to="/users/new">
//                                                 Update
//                                             </Button>
//                                             <Button variant="contained" color="secondary" className={classes.button} component={ Link } to="/users/new">
//                                                 Delete
//                                             </Button>
//
//                                         </TableCell>
//                                     </TableRow>
//                                 );
//                             })}
//                             {emptyRows > 0 && (
//                                 <TableRow style={{ height: 48 * emptyRows }}>
//                                     <TableCell colSpan={6} />
//                                 </TableRow>
//                             )}
//                         </TableBody>
//                     </Table>
//                 </div>
//             </Paper>
//         );
//     }
// }
//
// export default withStyles(styles)(UserTable);
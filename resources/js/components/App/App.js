import React from 'react';

import HeaderContainer from '../../containers/HeaderContainer';
import Content from '../Content/Content';
import NavBarContainer from '../../containers/NavBarContainer';

const App = () => {
    return (
        <div style={ { display: 'flex', flexDirection: 'column', height: '100vh' } }>
            <HeaderContainer />
            <div style={ { display: 'flex', flexDirection: 'row' } }>
                <NavBarContainer />
                <Content />
            </div>
        </div>
    );
};

export default App;

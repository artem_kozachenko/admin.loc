import React from 'react';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import axios from 'axios';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { composeWithDevTools } from 'redux-devtools-extension';

import { indexReducer } from '../store/indexReducer';

const persistConfig = {
    key: 'root',
    storage,
    blacklist: [
        'form',
        'indexUsers'
    ]
};
const composeEnhancers = composeWithDevTools({
    trace: true,
    traceLimit: 25
});
const persistedReducer = persistReducer(persistConfig, indexReducer);
const store = createStore(
    persistedReducer,
    { indexUsers: {present: { currentPage: 1, pageCounter: 1, users: [] } } },
    composeEnhancers(
        applyMiddleware(
            thunk.withExtraArgument(axios)
        )
    )
);
const persistor = persistStore(store);
const StoreConfig = { store, persistor };

export default StoreConfig;
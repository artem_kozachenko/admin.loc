<?php

namespace Tests\Unit\Utiles\Fractal;

use Tests\TestCase;
use App\Services\User\User\DB\Model\User;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Utiles\Fractal\Fractal\FractalInterface;
use App\Utiles\Fractal\Response\FractalResponse;
use App\Utiles\Fractal\Transformers\Transformer;
use Spatie\Fractalistic\Fractal as SpatieFractal;
use App\Utiles\Fractal\Paginator\PaginatorInterface;
use App\Utiles\Fractal\Serializers\SerializerInterface;

/**
 * Class FractalResponseTest
 * @package Tests\Unit\Utiles\Fractal
 */
final class FractalResponseTest extends TestCase
{
    /**
     * @var FractalResponse
     */
    private $testObject;

    /**
     * @var array
     */
    private $additionalOptions = [
        'includes' => [],
        'excludes' => [],
        'meta' => []
    ];

    private $paginator;
    private $serializer;
    private $transformer;
    private $fractalObject;
    private $fractalBuilder;

    public function setUp()
    {
        parent::setUp();

        $this->paginator = $this->createMock(PaginatorInterface::class);
        $this->fractalBuilder = $this->createMock(FractalInterface::class);
        $this->transformer = $this->createMock(Transformer::class);
        $this->serializer = $this->createMock(SerializerInterface::class);
        $this->fractalObject = $this->createMock(SpatieFractal::class);
        $this->testObject = $this->getTestObject();
        $this->fractalBuilder
            ->expects($this->once())
            ->method('create')
            ->willReturn($this->fractalObject);
        $this->fractalObject
            ->expects($this->once())
            ->method('toArray')
            ->willReturn([]);
    }

    /**
     * @dataProvider responseDifferentDataTypeProvider
     * @param $data
     */
    public function testResponseDifferentDataType($data)
    {
        $this->testObject->response($data, $this->transformer, $this->serializer);
    }

    public function testResponsePaginator()
    {
        $data = new LengthAwarePaginator(collect(), 0, 10, 1);
        $this->fractalObject->expects($this->once())->method('paginateWith')->willReturn($this->fractalObject);

        $this->testObject->response($data, $this->transformer, $this->serializer);
    }

    public function testResponseAdditionalOptionsSet()
    {
        $this->fractalObject->expects($this->once())->method('parseIncludes');
        $this->fractalObject->expects($this->once())->method('parseExcludes');
        $this->fractalObject->expects($this->once())->method('addMeta');

        $this->testObject->response(
            [],
            $this->transformer,
            $this->serializer,
            $this->additionalOptions
        );
    }

    public function testResponseAdditionalOptionsDontSet()
    {
        $this->fractalObject->expects($this->never())->method('parseIncludes');
        $this->fractalObject->expects($this->never())->method('parseExcludes');
        $this->fractalObject->expects($this->never())->method('addMeta');

        $this->testObject->response(
            [],
            $this->transformer,
            $this->serializer
        );
    }

    /**
     * @return array
     */
    public function responseDifferentDataTypeProvider()
    {
        $this->createApplication();
        $user = factory(User::class)->make();

        return [
            [$user],
            [$user->toArray()],
            [collect($user->toArray())],
        ];
    }

    /**
     * @return FractalResponse
     */
    private function getTestObject(): FractalResponse
    {
        return new FractalResponse($this->fractalBuilder, $this->paginator);
    }

    public function tearDown()
    {
        unset(
            $this->paginator,
            $this->serializer,
            $this->testObject,
            $this->fractalObject,
            $this->fractalBuilder,
            $this->transformer
        );

        parent::tearDown();
    }
}

<?php

namespace Tests\Unit\Utiles\Fractal;

use Tests\TestCase;
use App\Utiles\Validator\Factory;
use App\Utiles\Fractal\Fractal\Fractal;
use App\Services\User\User\DB\Model\User;
use App\Utiles\Fractal\Transformers\Transformer;
use Spatie\Fractalistic\Fractal as LaravelFractal;
use App\Utiles\ConfigUploader\ConfigUploaderInterface;
use App\Utiles\Validator\Interfaces\ValidatorInterface;
use App\Utiles\Fractal\Serializers\SerializerInterface;

/**
 * Class FractalTest
 * @package Tests\Unit\Utiles\Fractal
 */
final class FractalTest extends TestCase
{
    /**
     * @var Fractal
     */
    private $testObject;

    private $factory;
    private $validator;
    private $serializer;
    private $transformer;
    private $configUploader;

    public function setUp()
    {
        parent::setUp();

        $this->transformer = $this->createMock(Transformer::class);
        $this->serializer = $this->createMock(SerializerInterface::class);
        $this->configUploader = $this->createMock(ConfigUploaderInterface::class);
        $this->validator = $this->createMock(ValidatorInterface::class);
        $this->factory = $this->createMock(Factory::class);

        $this->configUploader->expects($this->once())->method('upload')->willReturn('string');
        $this->factory->expects($this->once())->method('getValidators')->willReturn($this->validator);

        $this->testObject = $this->getTestObject();
    }

    /**
     * @dataProvider providerCorrectDataType
     * @param $data
     * @throws \App\Utiles\Validator\Exceptions\ValidatorException
     * @throws \App\Utiles\Fractal\Exception\FractalException
     */
    public function testCreateCorrectDataTypes($data)
    {
        $this->validator->expects($this->once())->method('validate')->willReturn(true);

        $response = $this->testObject->create(
            $data,
            $this->transformer,
            $this->serializer,
            true
        );

        $this->assertInstanceOf(LaravelFractal::class, $response);
    }

    /**
     * @dataProvider providerIncorrectDataType
     * @param $data
     * @throws \App\Utiles\Validator\Exceptions\ValidatorException
     * @throws \App\Utiles\Fractal\Exception\FractalException
     * @expectedException \App\Utiles\Fractal\Exception\FractalException
     */
    public function testCreateIncorrectDataTypes($data)
    {
        $this->validator->expects($this->once())->method('validate')->willReturn(false);

        $this->testObject->create(
            $data,
            $this->transformer,
            $this->serializer,
            true
        );
    }

    public function testCreateIncludeLinks()
    {
        $this->validator->expects($this->once())->method('validate')->willReturn(true);
        $this->serializer->expects($this->once())->method('setBaseUrl');

        $this->testObject->create(
            [],
            $this->transformer,
            $this->serializer,
            true
        );
    }

    public function testCreateDontIncludeLinks()
    {
        $this->validator->expects($this->once())->method('validate')->willReturn(true);
        $this->serializer->expects($this->never())->method('setBaseUrl');

        $this->testObject->create(
            [],
            $this->transformer,
            $this->serializer,
            false
        );
    }

    /**
     * @return Fractal
     */
    private function getTestObject(): Fractal
    {
        return new Fractal($this->factory, $this->configUploader);
    }

    /**
     * @return array
     */
    public function providerCorrectDataType()
    {
        $this->createApplication();

        return [
            [[]],
            [factory(User::class)->make()],
            [collect()],
        ];
    }

    /**
     * @return array
     */
    public function providerIncorrectDataType()
    {
        return [
            [''],
            [1],
        ];
    }

    public function tearDown()
    {
        unset(
            $this->testObject,
            $this->factory,
            $this->validator,
            $this->serializer,
            $this->transformer,
            $this->configUploader
        );

        parent::tearDown();
    }
}
